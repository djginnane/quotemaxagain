<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;
use Quotemax\DashboardBundle\Entity\Quote;
use Quotemax\DashboardBundle\Form\Type\QuoteType;
use Quotemax\DashboardBundle\Entity\Item;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Quotemax\DashboardBundle\Entity\ItemOption;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/email", name="qmxDashboard_email")
 * 
 * Note: to handle generate other email + pdf tasks only
 * 
 */
class EmailController extends Controller
{
	

	public function getUserRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxUserBundle:User');
	}
	
	public function getEntityManager(){
		return $this->getDoctrine()->getManager();
	}
	
	public function getQuoteManager(){
		return $this->get('quotemax_dashboard.quote_manager');
	}
	
	public function getQuoteRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Quote')	;
	}
	
	public function getCategoryRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Category');
	}
	
	public function getCurrencyRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Currency');
	}
	
	public function getProductRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Product');
	}
	
	public function getVariableRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Variable');
	}
	
	public function getTransportRateRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:TransportRate');
	}
	
	private function randomString($length = 10) {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
	
		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}
	
		return $key;
	}
	
	private function roundUpHalf($weight){
		$weightTmp = (ceil($weight * 2))/2;
	
		//var_dump('weight: '.$weight.' = '.$weightTmp);
	
		return $weightTmp;
	}
	
	
	
	
	public function getFlash(){
		return $this->get('braincrafted_bootstrap.flash');
	}
	
	public function getUser(){
		return $this->get('security.context')->getToken()->getUser(); 
	}
	
	public function getCompany(){
		$company = null;
		$user = $this->getUser();
		if($user->getDetail()){
			if($user->getDetail()->getCompany()){
				$company = $user->getDetail()->getCompany();
			}
		}
		return $company;
	}
	
	
	
	public function forAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function isValidCompanyToProceed(Quote $quote){
		//if($this->getUser())
		if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			return true;
		}else{
			if($quote->getCompany()->getId() != $this->getCompany()->getId()){
				throw new AccessDeniedException('Unable to access this item.');
				return false;
			}
			return false;
		}
	}
	

	
	
	/**
	 * @Route("/", name="qmxDashboard_email_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {

    	 return $this->redirect($this->generateUrl('qmxDashboard_quote_index'));
    
    }
    

    /**
     * @Route("/pdf/invoice/{id}", name="qmxDashboard_email_pdf_invoice",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function pdfInvoiceAction(Request $request, $id)
    {
    	
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}  	
    	$this->isValidCompanyToProceed($quote);
    	
    	$debug = $this->get('request')->get('_debug','no');
    	
    	$attachment = $this->get('request')->get('attachment','no');
    	
    	/* ** create PDF and images in Symfony2
    	 * 		- https://github.com/KnpLabs/KnpSnappyBundle
    	 * 		- http://wkhtmltopdf.org/
    	 */
    	$filename = 'Invoice-'.$quote->getCode().'.pdf';
    	
    	$options = array();
    	$options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	//calculate 'Due' date
    	$dueDate = new \DateTime();
    	$dueDate->setTime(0, 1, 0);
    	$numOfDeliveryDays = array_key_exists('%order-delivery-period%', $options['tokens']) ?
											    	$options['tokens']['%order-delivery-period%'] :
											    	15;
    	//TODO: client request 'DueDate' of invoice to be current date by default
    	//$dueDate->modify('+'.$numOfDeliveryDays.' day');
    	$options['dueDate'] = $dueDate;
    	
    	//render a pdf document as response from a controller
		$html = $this->renderView('QuotemaxDashboardBundle:Email:pdfInvoice.html.twig', array(
		    'quote'  => $quote,
			'options' => $options
		));
		
		if($debug == 'yes'){
			$this->forAdminOnly();
			//debug
			return new Response($html);
		}
		
		if($attachment == 'yes'){
			$pathPdf = $this->container->getParameter('quotemax.path_pdf');
			$pathPdf = $pathPdf."/".$this->randomString()."/".$filename;
			$this->get('knp_snappy.pdf')->generateFromHtml(
					$html,
					$pathPdf
			);
			//return new Response ($pathPdf);
			$response = new JsonResponse();
			$response->setData(array(
					'data' => $pathPdf
			));
			return $response;

		}else{
			return new Response(
					$this->get('knp_snappy.pdf')->getOutputFromHtml($html),
					200,
					array(
							'Content-Type'          => 'application/pdf',
							'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
					)
			);
		}
		

		
    }   
     
  


    /**
     * @Route("/send/invoice/{id}", name="qmxDashboard_email_send_invoice",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function sendInvoiceAction(Request $request, $id)
    {
    	
    	/*
    	if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
    		throw new AccessDeniedException();
    	}
    	*/
    	
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	

    	$kernel = $this->container->get('kernel');
    	//TODO: dynamically choose attach logo according to product id
    	$pathImage = $kernel->locateResource('@QuotemaxDashboardBundle/Resources/public/images/logo_vsignpro.png');
    	
    	//TOCONTINUE!!!!!!
    	
    	$filename = '#'.$quote->getCode().'';
    	$subject = $translator->trans("Order No")." ".$quote->getCode()." - ".$translator->trans("Confirmed");
    	$htmlBody = $this->renderView(
    					'QuotemaxDashboardBundle:Email:sendInvoice.html.twig',
    					array('quote'  => $quote)
    			);
    	
    	
    	
    	//get email from user/company
    	$toEmail = $quote->getCompany()->getEmail();
    	
    	//send email
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	$fromEmail = $tokens['%email-from%'];
    	$bccEmails = array_filter(explode(';', $tokens['%emails-bcc%']));
    	$message = \Swift_Message::newInstance()
    	->setSubject($subject)
    	//->setFrom('nattapon@ascentec.net') 
    	->setFrom($fromEmail)
    	->setTo($toEmail)
    	//->setBcc('nattapon@ascentec.net')
    	->setBcc($bccEmails) 
    	;
    	
    	//set body
    	//$imgLogo = '<br><center><img src="'.$message->embed(\Swift_Image::fromPath($pathImage)) .'" alt="logo-image" class="logo-pdf" /></center>';
    	$message
    	->setBody(
    			
    			/*$imgLogo.*/$htmlBody
    	)
    	;
    	$message->setContentType('text/html');
    	
    	//create attachment + attach it to the message
    	//check if response is ok 200 or not, if not then exception
    	$resp = $this->forward('QuotemaxDashboardBundle:Email:pdfInvoice', array(
    			'id'  => $quote->getId(),
    			'attachment' => 'yes',
    	));
    	$json = null;

    	if ($resp->getStatusCode() == 200){
    		$json = json_decode($resp->getContent(), true);
    		$attachment = \Swift_Attachment::fromPath($json['data']);
    		$message->attach($attachment);
    	}else{
    		//TODO: add log / call exception...
    	}
    	
    	 
    	$result = $this->get('mailer')->send($message);
    	
    	//delete generated pdf
    	if($result){
    		if($json){
    			//TODO:
    			// - probably create another action&route to just remove pdf file
    			//unlink($json['data']);
    			
    		}
    		$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    		$this->getFlash()->alert($ack);
    		
    	}else{
    		if($json){
    			//unlink($json['data']);
    		}
    		$ack = $translator->trans("Failed to send email to")." ".$toEmail;
    		$this->getFlash()->alert($ack);
    	}
    	
    	//send another email to confirm with internal admin only
    	$resp = $this->forward('QuotemaxDashboardBundle:Email:sendInvoiceAdminonly', array(
    			'id'  => $quote->getId()
    	));
    	
    	//$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    	//$this->getFlash()->alert($ack);
    	
    	
    	
    	 
    	return new Response('Email Quote!! <br>'.' '.$pathImage.' '.$htmlBody);
    }
    
    
    /**
     * @Route("/send/invoice/adminonly/{id}", name="qmxDashboard_email_send_invoice_adminonly",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function sendInvoiceAdminonlyAction(Request $request, $id)
    {
    	
    	 
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    
    	$kernel = $this->container->get('kernel');
    	//TODO: dynamically choose attach logo according to product id
    	$pathImage = $kernel->locateResource('@QuotemaxDashboardBundle/Resources/public/images/logo_vsignpro.png');
    	 
 
    	 
    	$filename = '#'.$quote->getCode().'';
    	$subject = $translator->trans("Order No")." ".$quote->getCode()." - ".$quote->getCompany()->getName()." - ".$translator->trans("Confirmed");
    	$htmlBody = $this->renderView(
    			'QuotemaxDashboardBundle:Email:sendInvoiceAdminonly.html.twig',
    			array('quote'  => $quote)
    	);
    	 
    	 
    	 
    	//get email from user/company
    	//$toEmail = $quote->getCompany()->getEmail();
    	 
    	//send email
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	$fromEmail = $tokens['%email-from%'];
    	$bccEmails = array_filter(explode(';', $tokens['%emails-bcc%']));
    	$toEmail = $bccEmails;
    	
    	$message = \Swift_Message::newInstance()
    	->setSubject($subject)
    	->setFrom($fromEmail)
    	->setTo($toEmail)
    	//->setBcc($bccEmails)
    	;
    	 
    	//set body
    	//$imgLogo = '<br><center><img src="'.$message->embed(\Swift_Image::fromPath($pathImage)) .'" alt="logo-image" class="logo-pdf" /></center>';
    	$message
    	->setBody(
    			 
    			/*$imgLogo.*/ $htmlBody
    	)
    	;
    	$message->setContentType('text/html');
    	 
    	
    
    	$result = $this->get('mailer')->send($message);
    	 
    	
    	 
    	//ToNote: no need to flash message for send email to internal Admin
    	//$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    	//$this->getFlash()->alert($ack);
    	 
    
    	return new Response('Email Quote!! <br>'.' '.$pathImage.' '.$htmlBody);
    }
    
    
    /**
     * @Route("/send/payment/confirmed/{id}", name="qmxDashboard_email_send_payment_confirmed",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function sendPaymentConfirmedAction(Request $request, $id)
    {
  	
    	$this->forAdminOnly();
    
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    
    
    	$kernel = $this->container->get('kernel');
    	
    	$pathImage = $kernel->locateResource('@QuotemaxDashboardBundle/Resources/public/images/logo_vsignpro.png');
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
   
    	
    	$filename = '#'.$quote->getCode().'';
    	$subject = $translator->trans("Order No")." ".$quote->getCode()." - ".$translator->trans("Payment confirmed");
    	$htmlBody = $this->renderView(
    			'QuotemaxDashboardBundle:Email:sendPaymentConfirmed.html.twig',
    			array('quote'  => $quote,
    					'tokens' => $tokens
    				)
    	);
    
    
    
    	//get email from user/company
    	$toEmail = $quote->getCompany()->getEmail();
    
    	//send email
    	
    	$fromEmail = $tokens['%email-from%'];
    	$bccEmails = array_filter(explode(';', $tokens['%emails-bcc%']));
    	 
    	$message = \Swift_Message::newInstance()
    	->setSubject($subject)
    	->setFrom($fromEmail)
    	->setTo($toEmail)
    	->setBcc($bccEmails)
    	;
    
    	//set body
    	//$imgLogo = '<br><center><img src="'.$message->embed(\Swift_Image::fromPath($pathImage)) .'" alt="logo-image" class="logo-pdf" /></center>';
    	$message
    	->setBody(
    
    			/*$imgLogo.*/ $htmlBody
    	)
    	;
    	$message->setContentType('text/html');
    
    	 
    
    	$result = $this->get('mailer')->send($message);
    
    	 
    
    	//ToNote: no need to flash message for send email to internal Admin
    	//$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    	//$this->getFlash()->alert($ack);
    
    
    	return new Response('Email Quote!! <br>'.' '.$pathImage.' '.$htmlBody);
    }
    
    
    
    /**
     * @Route("/send/delivery/confirmed/{id}", name="qmxDashboard_email_send_delivery_confirmed",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function sendDeliveryConfirmedAction(Request $request, $id)
    {
    	 
    	$this->forAdminOnly();
    
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    
    
    	$kernel = $this->container->get('kernel');
    	 
    	$pathImage = $kernel->locateResource('@QuotemaxDashboardBundle/Resources/public/images/logo_vsignpro.png');
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	 
    	 
    	
    	$subject = $translator->trans("Order No")." ".$quote->getCode()." - ".$translator->trans("Shipment confirmed, Tracking No.")."".$quote->getTrackingNo();
    	$htmlBody = $this->renderView(
    			'QuotemaxDashboardBundle:Email:sendDeliveryConfirmed.html.twig',
    			array('quote'  => $quote,
    					'tokens' => $tokens
    			)
    	);
    
    
    
    	//get email from user/company
    	$toEmail = $quote->getCompany()->getEmail();
    
    	//send email
    	 
    	$fromEmail = $tokens['%email-from%'];
    	$bccEmails = array_filter(explode(';', $tokens['%emails-bcc%']));
    
    	$message = \Swift_Message::newInstance()
    	->setSubject($subject)
    	->setFrom($fromEmail)
    	->setTo($toEmail)
    	->setBcc($bccEmails)
    	;
    
    	//set body
    	//$imgLogo = '<br><center><img src="'.$message->embed(\Swift_Image::fromPath($pathImage)) .'" alt="logo-image" class="logo-pdf" /></center>';
    	$message
    	->setBody(
    
    			/*$imgLogo.*/ $htmlBody
    	)
    	;
    	$message->setContentType('text/html');
    
    
    
    	$result = $this->get('mailer')->send($message);
    
    
    
    	//ToNote: no need to flash message for send email to internal Admin
    	//$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    	//$this->getFlash()->alert($ack);
    
    
    	return new Response('Email Quote!! <br>'.' '.$pathImage.' '.$htmlBody);
    }
    
    
    
    
    /**
     * @Route("/test/{id}", name="qmxDashboard_email_test",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function testAction(Request $request, $id)
    {
    
    	$this->forAdminOnly();
    	
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
  
    	
    	$this->isValidCompanyToProceed($quote);
    	
    	$attachment = $this->get('request')->get('attachment','no');
    	
    	/* ** create PDF and images in Symfony2
    	 * 		- https://github.com/KnpLabs/KnpSnappyBundle
    	 * 		- http://wkhtmltopdf.org/
    	 */
    	$filename = 'quote#'.$quote->getId().'-'.$quote->getCode().'.pdf';
    	
    	//render a pdf document as response from a controller
		$html = $this->renderView('QuotemaxDashboardBundle:Email:invoiceTemplate.html.twig', array(
		    'quote'  => $quote
		));

		
		if($attachment == 'yes'){
			$pathPdf = $this->container->getParameter('quotemax.path_pdf');
			$pathPdf = $pathPdf."/".$this->randomString()."/".$filename;
			$this->get('knp_snappy.pdf')->generateFromHtml(
					$html,
					$pathPdf
			);
			//return new Response ($pathPdf);
			$response = new JsonResponse();
			$response->setData(array(
					'data' => $pathPdf
			));
			return $response;

		}else{
			return new Response(
					$this->get('knp_snappy.pdf')->getOutputFromHtml($html),
					200,
					array(
							'Content-Type'          => 'application/pdf',
							'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
					)
			);
		}
    
    }
    
    
    
    
}
