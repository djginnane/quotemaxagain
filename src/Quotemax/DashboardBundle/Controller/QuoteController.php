<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;
use Quotemax\DashboardBundle\Entity\Quote;
use Quotemax\DashboardBundle\Form\Type\QuoteType;
use Quotemax\DashboardBundle\Entity\Item;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Quotemax\DashboardBundle\Entity\ItemOption;
use Symfony\Component\HttpFoundation\JsonResponse;
use Assetic\Exception\Exception;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Quotemax\DashboardBundle\Enum\DesignTypeEnum;
use Quotemax\DashboardBundle\Enum\QuoteStatusEnum;

/**
 * @Route("/quote", name="qmxDashboard_quote")
 */
class QuoteController extends Controller
{
	
	
	/* 
	 * **Handling Error Exception:::
	 * 		- http://jameshalsall.co.uk/posts/displaying-exception-messages-in-symfony-production-environments << 'Displaying Exception Messages in Symfony Production Environments'
	 * 
	 * **KnpSnappy PDF for WkHtmlToPdf
	 *		- http://stackoverflow.com/questions/23650539/wkhtmltopdf-knp-snappy-custom-height-width-which-unit-is-expected
	 *		- http://stackoverflow.com/questions/11859872/wkhtmltopdf-encoding-issue
	 *	
	 * **How to access a different controller from inside a controller Symfony2
	 * 		- http://stackoverflow.com/questions/15827384/how-to-access-a-different-controller-from-inside-a-controller-symfony2
	 * 		- http://symfony.com/doc/master/cookbook/controller/service.html  <<< 'How to Define Controllers as Services' 
	 *
	 *
	 *
	 * Common Controller Tasks::: 
	 *      - http://symfony.com/doc/current/book/controller.html#common-controller-tasks
	 * 
	 * Managing the session
	 * 		-http://symfony.com/doc/current/book/controller.html#managing-the-session
	 * 
	 * FrameworkExtraBundle documentation:::
	 * 		- http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/view.html
	 * 
	 * Serving Files:
	 * 		- http://symfony.com/doc/current/components/http_foundation/introduction.html#component-http-foundation-serving-files
	 * 
	 * Securing Methods Using Annotations:
	 * 		- https://github.com/schmittjoh/JMSSecurityExtraBundle
	 */

	
	public function getUserRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxUserBundle:User');
	}
	
	public function getEntityManager(){
		return $this->getDoctrine()->getManager();
	}
	
	public function getQuoteManager(){
		return $this->get('quotemax_dashboard.quote_manager');
	}
	
	public function getQuoteRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Quote');
	}
	
	public function getCategoryRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Category');
	}
	
	public function getCurrencyRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Currency');
	}
	
	public function getProductRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Product');
	}
	
	public function getVariableRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Variable');
	}
	
	public function getTransportRateRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:TransportRate');
	}
	
	public function forAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function forSuperAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	
	
	public function getFlash(){
		return $this->get('braincrafted_bootstrap.flash');
	}
	
	public function getUser(){
		return $this->get('security.context')->getToken()->getUser(); 
	}
	
	public function getCompany(){
		$company = null;
		$user = $this->getUser();
		if($user->getDetail()){
			if($user->getDetail()->getCompany()){
				$company = $user->getDetail()->getCompany();
			}
		}
		return $company;
	}
	
	public function preUpdateQuote(Quote $quote){
		
		//set current rate
		//TODO: switch back later
		//$currencyCalculation = $this->container->getParameter('currency_calculation');
		$currencyCalculation = 'thb';
		$currencySelected = $quote->getCurrency();
		
		$currencyRepo = $this->getCurrencyRepository();
		
		$currency = $currencyRepo->findOneBy(array('fromCurrency' => $currencyCalculation, 
													'toCurrency' => $currencySelected));
		
		if (!$currency) {
			throw $this->createNotFoundException(
					'No currency rate found.'
			);
		}
		
		$quote->setCurrencyRate($currency->getRate());
		
		//generate quotation #
		if(!$quote->getId()){
			
			//$codeNumberInitial = $this->container->getParameter('quotemax.clientCodeInitial');
			//$options = array('codeNumberInitial' => $codeNumberInitial);
			
			$code = $this->getQuoteRepository()->generateNewCode($quote/*, $options*/);
			$quote->setCode($code);
		}
		
		
	}
	
	
	
	public function isValidCompanyToProceed(Quote $quote){
		//if($this->getUser())
		if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			return true;
		}else{
			if($quote->getCompany()->getId() != $this->getCompany()->getId()){
				throw new AccessDeniedException('Unable to access this item.');
				return false;
			}
			return false;
		}
	}
	
	
	private function randomString($length = 10) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));
	
	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }
	
	    return $key;
	}
	
	private function roundUpHalf($weight){
		$weightTmp = (ceil($weight * 2))/2;
		
		//var_dump('weight: '.$weight.' = '.$weightTmp);
		
		return $weightTmp;
	}
	
	
	/**
	 * @Route("/", name="qmxDashboard_quote_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {

    	 return $this->redirect($this->generateUrl('qmxDashboard_quote_create'));
    
        // Managing Errors and 404 Pages
        //if (!$product) {
        //	throw $this->createNotFoundException('The product does not exist');
        //}
        
        // Returning a 500 HTTP response code
        // throw new \Exception('Something went wrong!');
    }
    
    /**
     * @Route("/list", name="qmxDashboard_quote_list")
     * @Template();
     */
    public function listAction(Request $request)
    {
    	$param = array();
    	$param['search-text'] = $request->query->get('search-text');
    	$param['search-type'] = $request->query->get('search-type');
   		
   		$layoutExcluded = $request->get('layoutExcluded', false);
   		$includedFromRoute = $request->get('includedFromRoute', 'qmxDashboard_quote_list');
   		
   		//filter quote list by each company + status
   		$options = array();
   		switch($includedFromRoute){
   			case 'qmxDashboard_quote_list':
   			case 'qmxDashboard_default_index':
   			case 'qmxDashboard_history_list':
   				$options['status'] = '';
   				break;
   			case 'qmxDashboard_order_list':
   				$options['status'] = 'PEN';
   				break;
   			default:
   			
   		}
   		
   		if (false !== $this->get('security.context')->isGranted('ROLE_ADMIN')) {
   			$options['company'] = '';
   		}else{
   			$options['company'] = $this->getCompany()->getId();
   		}
   		

    	$options['param'] = $param;
    	$query = $this->getQuoteRepository()->searchItemInQuery($options);
    	
    	$page = 1;
    	if($this->get('request')->query->get('page')){
    		$page = $this->get('request')->query->get('page', 1);
    	}else{
    		$page = $this->get('request')->get('page',1);
    	}
    	if(empty($page)){
    		$page=1;
    	}
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    			$query,
    			$page, /*page number*/
    			10 /*limit per page*/
    		);
    	
    	
    	return array('pagination' => $pagination, 'layoutExcluded' => $layoutExcluded, 'includedFromRoute' => $includedFromRoute);
    }
    
    
    /**
     * @Route("/create", name="qmxDashboard_quote_create")
     * @Template();
     */
    public function createAction(Request $request)
    {
    	set_time_limit(0);
    	
    	//translator
    	$translator = $this->get('translator');
    	
    	//$request->query->get('myParam'); // get a $_GET parameter
    	//$request->request->get('myParam'); // get a $_POST parameter
    	
    	//get product type
    	$pid = $request->get('pid', 1); // '1' id as default 'Acrylic'
    	$product = $this->getProductRepository()->find($pid);
    	
    	if (!$product) {
    		throw $this->createNotFoundException(
    				'No Product found for id '.$pid
    		);
    	}
    	
    	
    	
    	//set new 'quote'
    	$quote = new Quote();    	
    	$this->presetNewQuote($quote, $product);
    	
    	$optionsForm = array();
    	
    	if($this->getCompany() && !in_array('ROLE_ADMIN', $this->getUser()->getRoles()) && !in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles())){
    		$optionsForm = array('companyId' => $this->getCompany()->getId());
    	}
    	
    	$form = $this->createForm(new QuoteType($optionsForm), $quote, array(
				    'action' => $this->generateUrl('qmxDashboard_quote_create', array('pid' => $pid)),
				    'method' => 'POST',
				));
    	
    	
    	$form->handleRequest($request);
    	
    	if ($form->isValid()) {
    		//var_dump($form);
    		//exit();
    		//var_dump($quote->getDetail()->getDocuments());
    		//return $this->render('QuotemaxDashboardBundle:Quote:preview.html.twig', array('form' => $form->createView(), 'quote' => $quote));
    		
    		$em = $this->getDoctrine()->getManager();

    		$this->preUpdateQuote($quote);
    		
    		$detail = $quote->getDetail();
    		$quote->setDetail(null);
    		
    		$em->persist($quote);
    		$em->flush();
    		
    		$quote->setDetail($detail);
    		
    		if($detail->getItems()){
    			foreach ($detail->getItems() as $item){
    				$item->setQuoteDetail($detail);
    				$item->setQuantity($quote->getQuantity());
    				$item->setCurrency($quote->getCurrency());
    				$item->setMeasureUnit($quote->getMeasureUnit());
    				$em->persist($item);
    				
    			}
    		}

    		$em->persist($detail);
    		$em->flush();
    		//proceed calculating of each items & set price
    		$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    		$transportRates = $this->getTransportRateRepository()->findAll();
    		$quote->calculatePrice($tokens, $transportRates);
    		$em->flush();
    		
    		$ack = $translator->trans("Successfully created quote.");
    		$this->getFlash()->success($ack);
    		
    		//send email + attached generated PDF file
    		$resp = $this->forward('QuotemaxDashboardBundle:Quote:Email', array(
    				'id'  => $quote->getId()
    		));
    	
    		//return $this->redirect($this->generateUrl('qmxDashboard_order_index'));
    		return $this->redirect($this->generateUrl('qmxDashboard_quote_confirm', array('id' => $quote->getId())));
    	}
		
    	return array(
    			'form' => $form->createView()
    	);

    }
    
    
    /**
     * @Route("/preview", name="qmxDashboard_quote_preview")
     * @Template();
     */
    public function previewAction(Request $request)
    {
    	//translator
    	$translator = $this->get('translator');
    	 
    	//$request->query->get('myParam'); // get a $_GET parameter
    	//$request->request->get('myParam'); // get a $_POST parameter
    	 
    	//get product type
    	$pid = $request->get('pid', 1); // '1' id as default 'Acrylic'
    	$product = $this->getProductRepository()->find($pid);
    	 
    	if (!$product) {
    		throw $this->createNotFoundException(
    				'No Product found for id '.$pid
    		);
    	}
    	 
    	 
    	 
    	//set new 'quote'
    	$quote = new Quote();
    	$this->presetNewQuote($quote, $product);
    	 
    	$optionsForm = array();
    	if($this->getCompany()){
    		$optionsForm = array('companyId' => $this->getCompany()->getId());
    	}
    	 
    	$form = $this->createForm(new QuoteType($optionsForm), $quote, array(
    			'action' => $this->generateUrl('qmxDashboard_quote_create', array('pid' => $pid)),
    			'method' => 'POST',
    	));
    	 
    	 
    	$form->handleRequest($request);
    	$this->preUpdateQuote($quote);
    	
    	
    	//debug
    	//var_dump($quote);
    	//exit();
    	
    	//if ($form->isValid()) {
    		//$em = $this->getDoctrine()->getManager();
    
    		$this->preUpdateQuote($quote);
    		
    		$detail = $quote->getDetail();
    		$quote->setDetail(null);
    		
    		//$em->persist($quote);
    		//$em->flush();
    
    		$quote->setDetail($detail);
    
    		if($detail->getItems()){
    			foreach ($detail->getItems() as $item){
    				$item->setQuoteDetail($detail);
    				$item->setQuantity($quote->getQuantity());
    				$item->setCurrency($quote->getCurrency());
    				$item->setMeasureUnit($quote->getMeasureUnit());
    				//$em->persist($item);
    
    			}
    		}
    
    		
    		
    		
    		//$em->persist($detail);
    		//$em->flush();
    		//proceed calculating of each items & set price
    		$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote] );
    		$transportRates = $this->getTransportRateRepository()->findAll();
    		$quote->calculatePrice($tokens, $transportRates);
    		//$em->flush();
    
    		//$ack = $translator->trans("Successfully created quote.");
    		//$this->getFlash()->success($ack);
    
    		//send email + attached generated PDF file
    		/*$resp = $this->forward('QuotemaxDashboardBundle:Quote:Email', array(
    				'id'  => $quote->getId()
    		));*/
    		 
    		//return $this->redirect($this->generateUrl('qmxDashboard_order_index'));
    	//}
    
    		$options = ['tokens' => $tokens];
    		
    	return array(
    			'form' => $form->createView(),
    			'quote' => $quote,
    			'templateInclude' => 'quotePreview',
    			'options' => $options
    	);
    
    }
    
    
    private function presetNewQuote(Quote $quote, Product $product){
    	$detail = $quote->getDetail();
    	$detail->setProduct($product);
    	if(count($product->getItemOptions())){
    		foreach($product->getItemOptions() as $itemOption){
    			$io= new ItemOption();
    			$io->setCategory($itemOption->getCategory());
    			$detail->addItemOption($io);
    		}
    		
    	}
    	
    	//set company
    	$quote->setCompany($this->getCompany());
    }
    
    
    /**
     * @Route("/generate/product", name="qmxDashboard_quote_generate_product",
     * 							requirements = {})
     * @Template("QuotemaxDashboardBundle:Quote:generateProduct.html.twig")
     */
    public function generateProductAction(Request $request)
    {	
    	
    	//get product type
    	$pid = $request->get('pid', 1); // '1' id as default 'Acrylic'
    	$product = $this->getProductRepository()->find($pid);
    	
    	if (!$product) {
    		throw $this->createNotFoundException(
    				'No Product found for id '.$id
    		);
    	}
    	
    	
    	//set new 'quote'
    	$quote = new Quote();    	
    	$this->presetNewQuote($quote, $product);
    	
    	
    	
    	$form = $this->createForm(new QuoteType(), $quote, array(
				    'action' => $this->generateUrl('qmxDashboard_quote_create', array('pid' => $pid)),
				    'method' => 'POST',
				));
    	 
    	
    	return array(
    			'form' => $form->createView()
    	);
    	 
    }  
    
    
    /**
     * @Route("/generate/item", name="qmxDashboard_quote_generate_item",
     * 							requirements = {})
     * @Template("QuotemaxDashboardBundle:Quote:generateItem.html.twig")
     */
    public function generateItemAction(Request $request)
    {
    	 
    	//get parameter
    	$letter = $request->get('letter', '');
    	$numOfGraphicItem = $request->get('numOfGraphicItem', 0);
    	 
    	$letter = preg_replace( '/\s+/', '', trim($letter) );
    	$numOfLetter = strlen($letter);
    	
    	
    	//get product type
    	$pid = $request->get('pid', 1); // '1' id as default 'Acrylic'
    	$product = $this->getProductRepository()->find($pid);
    	 
    	if (!$product) {
    		throw $this->createNotFoundException(
    				'No Product found for id '.$pid
    		);
    	}
    	
    	 
    	//set new 'quote'
    	$quote = new Quote();
    	$this->presetNewQuote($quote, $product);

    	//set items according to number of letters + graphic logos
    	$detail = $quote->getDetail();
    	for ($i=1 ; $i <= $numOfLetter; $i++){
    		$item = new Item();
    		$item->setName('Letter # '.$i. ' - \''.$letter[$i-1].'\'');
    		$item->setType(DesignTypeEnum::LETTER);
    		$detail->addItem($item);
    	}
    	

    	for ($i=1 ; $i <= $numOfGraphicItem; $i++){
    		$item = new Item();
    		$item->setName('GraphicElement # '.$i);
    		$item->setType(DesignTypeEnum::LOGO);
    		$detail->addItem($item);
    	}
    	
    	 
    	 
    	$form = $this->createForm(new QuoteType(), $quote, array(
    			'action' => $this->generateUrl('qmxDashboard_quote_create', array('pid' => $pid)),
    			'method' => 'POST',
    	));
    
    	 
    	return array(
    			'form' => $form->createView()
    	);
    
    }
    
    
    /**
     * @Route("/update/{id}", name="qmxDashboard_quote_update",
     * 							requirements = {"id" = "\d+"})
     * @Template("QuotemaxDashboardBundle:Quote:create.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	set_time_limit(0);
    	
    	$this->forSuperAdminOnly();
    	
    	//translator
    	$translator = $this->get('translator');
    	
    	$quote = $this->getQuoteRepository()->find($id);
	
	    if (!$quote) {
	        throw $this->createNotFoundException(
	            'No Quote found for id '.$id
	        );
	    }
	    
	    
	    if ($quote->getStatus() == 'CON' || $quote->getStatus() == 'EXP') { //unable to edit if quote with 'CONFIRMED' or 'EXPIRED' status
	    	throw $this->createAccessDeniedException(
	    			'Unable to update this quote with confirmed / expired status'
	    	);
	    }
	  
	    $form = $this->createForm(new QuoteType(), $quote, array(
				    'action' => $this->generateUrl('qmxDashboard_quote_update', array('id' => $id)),
				    'method' => 'POST',
				));
	    
	    
	    $form->handleRequest($request);
	    if ($form->isValid()) {
	    	$em = $this->getEntityManager();
	    	
	    	$detail = $quote->getDetail();
	    	
	    	if($detail->getItems()){
	    		foreach ($detail->getItems() as $item){
	    			$item->setQuoteDetail($detail);
	    			$item->setQuantity($quote->getQuantity());
	    			$item->setCurrency($quote->getCurrency());
	    			$item->setMeasureUnit($quote->getMeasureUnit());
	    			$em->persist($item);
	    	
	    		}
	    	}
	    	
	    	
	    	//proceed calculating of each items & set price
	    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
	    	$transportRates = $this->getTransportRateRepository()->findAll();
	    	$quote->calculatePrice($tokens, $transportRates);
	    
	    
	    	$em->flush();
	    
	    	
	    	$ack = $translator->trans("Successfully updated quote.");
	    	$this->getFlash()->success($ack);
	    	
	    	
	    	//send email + attached generated PDF file
	    	$resp = $this->forward('QuotemaxDashboardBundle:Quote:Email', array(
	    			'id'  => $quote->getId()
	    	));
	    	
	    	return $this->redirect($this->generateUrl('qmxDashboard_quote_confirm', array('id' => $quote->getId())));
	    	//return $this->redirect($this->generateUrl('qmxDashboard_order_index'));
	    }
	    
	    return array(
	    		'form' => $form->createView(),
    			'quote' => $quote
	    );
	    
    }
    
    /**
     * @Route("/delete/{id}", name="qmxDashboard_quote_delete",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
    	$this->forAdminOnly();
    	
    	//translator
    	$translator = $this->get('translator');
    	
    	$quote = $this->getQuoteRepository()->find($id);
	
	    if (!$quote) {
	        throw $this->createNotFoundException(
	            'No Quote found for id '.$id
	        );
	    }
	    
	    if ($quote->getStatus() == 'CON') { //unable to delete if quote with 'CONFIRMED' status
	    	throw $this->createAccessDeniedException(
	    			'Unable to delete this quote with confirmed status'
	    	);
	    }
	    
	   
	    
	    $this->getEntityManager()->remove($quote);
	    $this->getEntityManager()->flush();
	    
	    $ack = $translator->trans("Successfully deleted quote.");
	    $this->getFlash()->alert($ack);
	    
	
	    return $this->redirect($this->generateUrl('qmxDashboard_history_index'));
    }
    
    
    /**
     * @Route("/view/{id}", name="qmxDashboard_quote_view",
     * requirements = {"id" = "\d+"})
     * @Template()
     *
     */
    public function viewAction($id)
    {
    	
    	$quote = $this->getQuoteRepository()->find($id);
	
	    if (!$quote) {
	        throw $this->createNotFoundException(
	            'No Quote found for id '.$id
	        );
	    }
	    
	    $this->isValidCompanyToProceed($quote);
    
	    $debug = $this->get('request')->get('_debug','no');
	    $auditLog = '';
	    //debug quote-calcuation engine
	    if($debug == 'yes'){
			$this->forSuperAdminOnly();
			
			//TODO:
			//	- round up closest to .5
			//	- use mod % from below to top to mactch catch
			//	- proabably seperate 3 main type of calculation rule by mod % to see fraction
			//  - create function to round up half .5 -*-
			//
			//
			
			
		    $auditLog = "";
		    $transportRates = $this->getTransportRateRepository()->findAll();
		    $tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
		    $quote->calculatePrice($tokens, $transportRates, $auditLog);
			
		    
	    }
	    
	    $options = array();
	    $options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	
	    return array('quote' => $quote, 'auditLog' => $auditLog, 'options' => $options);
    }
    
    
    /**
     * @Route("/confirm/{id}", name="qmxDashboard_quote_confirm",
     * requirements = {"id" = "\d+"})
     * @Template()
     *
     */
    public function confirmAction($id)
    {
    	 
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    	$this->isValidCompanyToProceed($quote);
 
    	//TODO: make tokens to be available in twig tempalte, so no need to pass it from controller every time
    	$options = array();
	    $options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	 
    	return array('quote' => $quote, 'options' => $options);
    }
    
    /**
     * @Route("/status/{id}/{status}", name="qmxDashboard_quote_status",
     * 							requirements={"id" = "\d+", "status" = "\w+"})
     * @Template()
     */
    public function statusAction(Request $request, $id, $status)
    {
    	//translator
    	$translator = $this->get('translator');
    	
    	$quote = $this->getQuoteRepository()->find($id);
    	/* @var $quote Quote */
    	
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}

    	$this->isValidCompanyToProceed($quote);
    	
    	$quote->setStatus($status);
    	$quote->setConfirmedAt( new \DateTime());
    	$em = $this->getEntityManager();
    	$em->flush($quote);

    	 
    	$ack = $translator->trans("Successfully updated quote with new status");
    	$this->getFlash()->success($ack);
    	 
    
    	return $this->redirect($this->generateUrl('qmxDashboard_history_index'));
    }
    
    
    /**
     * @Route("/statusconfirm/{id}", name="qmxDashboard_quote_status_confirm",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function statusConfirmAction(Request $request, $id)
    {
    	set_time_limit(0);
    	
    	//translator
    	$translator = $this->get('translator');
    	 
    	$quote = $this->getQuoteRepository()->find($id);
    	$status = QuoteStatusEnum::CONFIRM; //confirmed order by client
    	
    	//validate
    	if($quote->getStatus() == QuoteStatusEnum::CONFIRM){ //To workaround issue of twice redirecting, when using forward() to call other controller method, it redirect back to itself again
    		return $this->redirect($this->generateUrl('qmxDashboard_quote_view_invoice', ['id' => $quote->getId()]));
    	}
    	if($quote->getStatus() != QuoteStatusEnum::PENDING){
    		throw new \Exception('Invalid status of the quote, must be \'PENDING\' in order to confirm quote.');
    	}
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    
    	$this->isValidCompanyToProceed($quote);
    	 
    	$quote->setStatus($status);
    	$em = $this->getEntityManager();
    	
    	try{
    		//ToDO:
    		//	- if still unable to solve this issue 'Redirect Twice by forword()' at PROD
    		//		then use ajax, if complete then redirect to invoice page!!!
    		//
	    	
	    	//send email for invoice form + attached generated PDF file
	    	$resp = $this->forward('QuotemaxDashboardBundle:Email:sendInvoice', array(
	    			'id'  => $quote->getId()
	    	));
	    	
	    	//by redirect to call other controller method internally, but not working
	    	//$resp = $this->redirect($this->generateUrl('qmxDashboard_email_send_invoice', ['id'  => $quote->getId()]));
	    	//$resp->send();
	    	
	    	//TEST###########
	    	/*$path = array('id'  => $quote->getId());
	    	$path['_controller'] = 'QuotemaxDashboardBundle:Email:sendInvoice';
	    	$subRequest = $this->container->get('request_stack')->getCurrentRequest()->duplicate([], null, $path);
	    	$this->container->get('http_kernel')->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
	    	*/
	    	//var_dump($subRequest);
	    	//exit();
	    	//#############
	    	
	    	//save any changes
	    	$em->flush($quote); //TEMP CLOSING
	    	$ack = $translator->trans("Successfully updated quote with new status");
	    	$this->getFlash()->success($ack);
	    	
	    	
    	} catch(Exception $e){
    		
    	}
    
    
    	return $this->redirect($this->generateUrl('qmxDashboard_quote_view_invoice', ['id' => $quote->getId()]));
    }
    
    /**
     * @Route("/statusconfirm/payment/{id}", name="qmxDashboard_quote_status_confirm_payment",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function statusConfirmPaymentAction(Request $request, $id)
    {
    	$this->forAdminOnly();
    	
    
    	
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    	/* @var $quote Quote */
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	
    	
    	$this->isValidCompanyToProceed($quote);
    
    	
    	//validate
    	if($quote->getStatusPayment()){
    		throw new \Exception('The payment of the quote was confirmed already.');
    	}elseif($quote->getStatus() != 'CON'){
    		throw new \Exception('Invalid status of the quote, must be \'CONFIRMED\' in order to proceed payment confirm');
    	}
    	
    	//update
    	$statusPayment = true;
    	$quote->setStatus(QuoteStatusEnum::PAID);
    	$quote->setStatusPayment($statusPayment);
    	$quote->setPaidAt( new \DateTime());
    	$em = $this->getEntityManager();
    	$em->flush($quote);
    
    
    	$ack = $translator->trans("Successfully updated quote with new payment status");
    	$this->getFlash()->success($ack);
    	 
    	//send email to confirm payment from client
    	$resp = $this->forward('QuotemaxDashboardBundle:Email:sendPaymentConfirmed', array(
    			'id'  => $quote->getId()
    	));
    	
    	//$resp = $this->redirect($this->generateUrl('qmxDashboard_email_send_payment_confirmed', ['id'  => $quote->getId()]));
    	
    	//get referer
    	$referer = $request->headers->get('referer');
    
    	return $this->redirect($referer);
    }
    
    
    
    /**
     * @Route("/statusdeliver", name="qmxDashboard_quote_status_deliver")
     * @Template()
     */
    public function statusDeiverAction(Request $request)
    {
    	$this->forAdminOnly();
    	
    	 
    	//translator
    	$translator = $this->get('translator');
    	
    	//get parameter
    	$id =  $request->query->get('id');
    	$trackingNo =  $request->query->get('tracking-no');
    
    	$quote = $this->getQuoteRepository()->find($id);
    	/* @var $quote Quote */
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    	 
    	$this->isValidCompanyToProceed($quote);
    
    	 
    	//validate
    	if(!$quote->getStatusPayment()){
    		throw new \Exception('The payment of the quote must be proceeded first');
    	}elseif($quote->getStatus() != QuoteStatusEnum::PAID){
    		throw new \Exception('Invalid status of the quote, must be \'PAID\' in order to proceed delivery confirmation');
    	}
    	 
    	
    	
    	//update
    	$quote->setStatus(QuoteStatusEnum::DELIVER);
    	$quote->setTrackingNo($trackingNo);
    	$quote->setDeliveredAt( new \DateTime());
    	$em = $this->getEntityManager();
    	$em->flush($quote);
    
    
    	$ack = $translator->trans("Successfully updated quote with new delivery status");
    	$this->getFlash()->success($ack);
    
    	//send email to confirm delivery to client with 'Tracking No'
    	$resp = $this->forward('QuotemaxDashboardBundle:Email:sendDeliveryConfirmed', array(
    			'id'  => $quote->getId()
    	));
    	 
    	
    	 
    	//get referer
    	$referer = $request->headers->get('referer');
    
    	return $this->redirect($referer);
    }
    
    
    
    /**
     * @Route("/pdf/{id}", name="qmxDashboard_quote_pdf",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function pdfAction(Request $request, $id)
    {
    	
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	
    	$this->isValidCompanyToProceed($quote);
    	
    	$attachment = $this->get('request')->get('attachment','no');
    	
    	/* ** create PDF and images in Symfony2
    	 * 		- https://github.com/KnpLabs/KnpSnappyBundle
    	 * 		- http://wkhtmltopdf.org/
    	 * 		- (install on linux) http://stackoverflow.com/questions/2273534/how-to-install-wkhtmltopdf-on-a-linux-based-web-server
    	 */
    	$filename = $quote->getCode().'.pdf';
    	
    	$options = array();
    	$options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	
    	//render a pdf document as response from a controller
		$html = $this->renderView('QuotemaxDashboardBundle:Quote:pdf.html.twig', array(
		    'quote'  => $quote,
			'options' => $options
		));

		
		if($attachment == 'yes'){
			$pathPdf = $this->container->getParameter('quotemax.path_pdf');
			$pathPdf = $pathPdf."/".$this->randomString()."/".$filename;
			$this->get('knp_snappy.pdf')->generateFromHtml(
					$html,
					$pathPdf
			);
			//return new Response ($pathPdf);
			$response = new JsonResponse();
			$response->setData(array(
					'data' => $pathPdf
			));
			return $response;

		}else{
			return new Response(
					$this->get('knp_snappy.pdf')->getOutputFromHtml($html),
					200,
					array(
							'Content-Type'          => 'application/pdf',
							'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
					)
			);
		}
		

		
    }   
     
    
    /**
     * @Route("/print/{id}", name="qmxDashboard_quote_print",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function printAction(Request $request, $id)
    {
    	 
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    	$this->isValidCompanyToProceed($quote);
    	
    	$options = array();
    	$options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	 
    	$filename = 'quote#'.$quote->getId().'-'.$quote->getCode().'.pdf';
    	 
    	//render a pdf document as response from a controller
    	$html = $this->renderView('QuotemaxDashboardBundle:Quote:pdf.html.twig', array(
    			'quote'  => $quote,
    			'options' => $options
    	));
    
    
    	return new Response($html);
    	 
    }
    

    /**
     * @Route("/email/{id}", name="qmxDashboard_quote_email",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function emailAction(Request $request, $id)
    {
    	/*
    	if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
    		throw new AccessDeniedException();
    	}
    	*/
    	
    	//translator
    	$translator = $this->get('translator');
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	
    	/*
    	 * **SMTP Settings for Outlook365 and Gmail 
    	 * 		- http://portal.smartertools.com/kb/a2862/smtp-settings-for-outlook365-and-gmail.aspx
    	 * 		- http://swiftmailer.org/docs/messages.html
    	 * 		- http://stackoverflow.com/questions/9500573/path-of-assets-in-css-files-in-symfony2
    	 * 		- http://symfony.com/doc/current/components/config/resources.html
    	 */
    	$kernel = $this->container->get('kernel');
    	$pathImage = $kernel->locateResource('@QuotemaxDashboardBundle/Resources/public/images/logo_vsignpro.png');
    	
    	$filename = $quote->getCode().'';
    	$htmlBody = $this->renderView(
    					'QuotemaxDashboardBundle:Quote:email.html.twig',
    					array('quote'  => $quote)
    			);
    	$subject = $translator->trans("Quotation No ").$filename; 
    	
    	//TODO:
    	//	- destroy PDF after sent
    	
    	//get email from user/company
    	$toEmail = $quote->getCompany()->getEmail();
    	
    	//send email
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	$fromEmail = $tokens['%email-from%'];
    	$bccEmails = array_filter(explode(';', $tokens['%emails-bcc%']));
    	$message = \Swift_Message::newInstance()
    	->setSubject($subject)
    	//->setFrom('nattapon@ascentec.net') 
    	->setFrom($fromEmail)
    	->setTo($toEmail)
    	//->setBcc('nattapon@ascentec.net')
    	->setBcc($bccEmails) 
    	;
    	
    	//set body
    	//$imgLogo = '<br><center><img src="'.$message->embed(\Swift_Image::fromPath($pathImage)) .'" alt="logo-image" class="logo-pdf" /></center>';
    	$message
    	->setBody(
    			
    			/*$imgLogo.*/$htmlBody
    	)
    	;
    	$message->setContentType('text/html');
    	
    	//create attachment + attach it to the message
    	//check if response is ok 200 or not, if not then exception
    	$resp = $this->forward('QuotemaxDashboardBundle:Quote:Pdf', array(
    			'id'  => $quote->getId(),
    			'attachment' => 'yes',
    	));
    	$json = null;

    	if ($resp->getStatusCode() == 200){
    		$json = json_decode($resp->getContent(), true);
    		$attachment = \Swift_Attachment::fromPath($json['data']);
    		$message->attach($attachment);
    	}else{
    		//TODO: add log / call exception...
    	}
    	
    	/*
    	if (is_writable($tmpDir = sys_get_temp_dir())) {
    	\Swift_Preferences::getInstance()->setTempDir($tmpDir)->setCacheType('array');
    	}
    	*/
    	$result = $this->get('mailer')->send($message);
    	
    	//delete generated pdf
    	if($result){
    		if($json){
    			//TODO:
    			// - probably create another action&route to just remove pdf file
    			//unlink($json['data']);
    			
    		}
    		$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    		$this->getFlash()->alert($ack);
    		
    	}else{
    		if($json){
    			//unlink($json['data']);
    		}
    		
    		$ack = $translator->trans("Failed to send email to")." ".$toEmail;
    		$this->getFlash()->error($ack);
    	}
    	
    	
    	//$ack = $translator->trans("Sent email attached with PDF to")." ".$toEmail;
    	///$this->getFlash()->alert($ack);
    	
    	 
    	return new Response('Email Quote!! <br>'.' '.$pathImage.' '.$htmlBody);
    }
    
    /**
     * @Route("/expire", name="qmxDashboard_quote_expire")
     * @Template()
     */
    public function expireAction(Request $request)
    {
    	//TODO: probably command line version how to call from service directly
    	
    	if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
    		throw new AccessDeniedException();
    	}
    
    	$options = array();
    	$ack = '';
    	$quoteManager = $this->getQuoteManager();
    	$result = $quoteManager->findPendingQuotesAndExpire($options, $ack);
    
    	return new Response('Proceeding expiring pending quotes older than valid days, <p>'.$ack."</p>");
    }   

    
    /**
     * @Route("/view/invoice/{id}", name="qmxDashboard_quote_view_invoice",
     * requirements = {"id" = "\d+"})
     * @Template()
     *
     */
    public function viewInvoiceAction($id)
    {
    	 
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    	$this->isValidCompanyToProceed($quote);
    
    	$debug = $this->get('request')->get('_debug','no');
    	$auditLog = '';
    	//debug quote-calcuation engine
    	if($debug == 'yes'){
    		$this->forAdminOnly();
    			
    		//TODO:
    		//	- round up closest to .5
    		//	- use mod % from below to top to mactch catch
    		//	- proabably seperate 3 main type of calculation rule by mod % to see fraction
    		//  - create function to round up half .5 -*-
    		//
    		//
    			
    			
    		$auditLog = "";
    		$transportRates = $this->getTransportRateRepository()->findAll();
    		$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    		$quote->calculatePrice($tokens, $transportRates, $auditLog);
    			
    
    	}
    	 
    	$options = array();
    	$options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	 
    	return array('quote' => $quote, 'auditLog' => $auditLog, 'options' => $options);
    }
    
    /**
     * @Route("/print/invoice/{id}", name="qmxDashboard_quote_print_invoice",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function printInvoiceAction(Request $request, $id)
    {
    
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    
    	$this->isValidCompanyToProceed($quote);
    	 
    	$options = array();
    	$options['tokens'] = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    
    	$filename = 'quote#'.$quote->getId().'-'.$quote->getCode().'.pdf';
    
    	//render a pdf document as response from a controller
    	$html = $this->renderView('QuotemaxDashboardBundle:Email:pdfInvoice.html.twig', array(
    			'quote'  => $quote,
    			'options' => $options
    	));
    
    
    	return new Response($html);
    
    }
    
    
    
    
    
    
    
    
    /**
     * @Route("/calculate/debug/{id}", name="qmxDashboard_quote_calculate_debug",
     * 							requirements={"id" = "\d+"})
     * @Template()
     */
    public function calculateDebugAction(Request $request, $id)
    {
    	if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
    		throw new AccessDeniedException();
    	}
    	 
    	 
    	$quote = $this->getQuoteRepository()->find($id);
    
    	if (!$quote) {
    		throw $this->createNotFoundException(
    				'No Quote found for id '.$id
    		);
    	}
    	 
    	//proceed calculating of each items & set price
    	$tokens = $this->getVariableRepository()->findTokens(['quote' => $quote]);
    	$transportRates = $this->getTransportRateRepository()->findAll();
    	$quote->calculatePrice($tokens, $transportRates);
    
    	return new Response('Debug Calculate Quote price!! Total Price is '. $quote->getPriceTotal());
    }
    
    
    
    
}
