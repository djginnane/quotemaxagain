<?php

namespace Quotemax\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Quotemax\DashboardBundle\Entity\Product;
use Quotemax\DashboardBundle\Entity\Category;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * @Route("/report", name="qmxDashboard_report")
 */
class ReportController extends Controller
{
	/*	
	 * 	generate chart/graph in Symfony
	 * 		- https://github.com/marcaube/ObHighchartsBundle
	 * 		- https://github.com/saadtazi/SaadTaziGChartBundle
	 * 
	 *  datepicker for bootstrap
	 *  	- http://www.eyecon.ro/bootstrap-datepicker/
	 *  	- http://tarruda.github.io/bootstrap-datetimepicker/
	 *  
	 */
	
	public function forAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function forSuperAdminOnly(){
		if (false === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
			throw new AccessDeniedException();
		}
	}
	
	public function getUserRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxUserBundle:User');
	}
	
	public function getEntityManager(){
		return $this->getDoctrine()->getManager();
	}
	
	public function getQuoteRepository(){
		return $this->getDoctrine()->getRepository('QuotemaxDashboardBundle:Quote');
	}

	/**
	 * @Route("/", name="qmxDashboard_report_index")
	 * @Method({"GET", "POST"})
	 * @Template();
	 */
    public function indexAction(Request $request)
    {

    	return $this->redirect($this->generateUrl('qmxDashboard_report_view'));
    
     
    }
    
    /**
     * @Route("/view", name="qmxDashboard_report_view")
     * @Template();
     */
    public function viewAction(Request $request)
    {
    	$this->forSuperAdminOnly();
    	
    	//translator
    	$translator = $this->get('translator');
    	
    	
    	//form
    	$today =  new \DateTime();
    	$fromDate =  $today->modify('-1 month');
    	$toDate =  new \DateTime();
    	$form = $this->createFormBuilder()
	    	->add('From', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd',
	    															'attr' => array('class' => 'datepicker'),
	    															'data' => $fromDate
	    															))
	    	->add('To', 'date', array('widget' => 'single_text',  'format' => 'yyyy-MM-dd', 
	    															'attr' => array('class' => 'datepicker'),
	    															'data' => $toDate
	    															))
	    	->add('Query', 'submit')
	    	->getForm();
    	
    	$form->handleRequest($request);
    	
    	$data = null;
    	$options = array();
    	
    	$data = $this->getQuoteRepository()->getReportData($form, $options);
    	
    	/*
    	if ($form->isValid()) {
			
    		$data = $this->getQuoteRepository()->getReportData($form, $options);

    	}else{
    		$data = $this->getQuoteRepository()->getReportData($form, $options);
    	}
    	*/
    	
    	//render charts
    	$charts = $this->renderCharts($data);
    	
    	$conversionRate = floatval($data['totalConfirmed'] /  $data['totalQuote']) * 100;
    	
    	
    	return array('chart' => $charts[0],
    				 'chart1' => $charts[1],
    				 'conversionRate' => $conversionRate,
    				 'form' => $form->createView(),
    	);
    }
    
    private function renderCharts($data = null){
    	//translator
    	$translator = $this->get('translator');
    	
    	// Chart #1 Column
    	$series = array(
    			array("type" => 'column', 'name' =>$translator->trans('Confirmed'), 'data'=> $data['statusConfirmed'] /*array(1,2,5,30)*/),
    			array("type" => 'column','name' =>$translator->trans('Unconfirmed'), 'data'=> $data['statusUnconfirmed']  /*array(1,2,20,50)*/),
    	
    	);
    	$categories = $data['categories']; /*array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');*/
    	
    	$ob = new Highchart();
    	$ob->chart->renderTo('column');  // The #id of the div where to render the chart
    	$ob->title->text($translator->trans('Monthly Quote'));
    	$ob->xAxis->title(array('text'  => $translator->trans("Month")));
    	$ob->xAxis->categories($categories);
    	$ob->yAxis->title(array('text'  => $translator->trans("Num Of Quotes")));
    	$ob->series($series);
    	//ToBeRemoved:: just for testing generating in PDF
    	$ob->plotOptions->column(array(
    				'enableMouseTracking' => false,
    				'animation' => false, //ToBeRemoved:: just for testing generating in PDF
    				'shadow' => false,
    				'stroke-width' => 22,
    				 ));
    	$ob->plotOptions->series(array(
    			'enableMouseTracking' => false, //ToBeRemoved:: just for testing generating in PDF
    			'animation' => false, //ToBeRemoved:: just for testing generating in PDF
    			'shadow' => false,
    	));
    	$ob->point->marker(array('enabled' => false));
    	$ob->tooltip->enable(false);
    	 
    	 
    	// Chart #2 Pie Chart
    	$ob1 = new Highchart();
    	$ob1->chart->renderTo('piechart');
    	$ob1->title->text($translator->trans('Conversion Rate'));
    	$ob1->plotOptions->pie(array(
    			'allowPointSelect'  => true,
    			'cursor'    => 'pointer',
    			'dataLabels'    => array('enabled' => false),
    			'showInLegend'  => true,
    			'enableMouseTracking' => false, //ToBeRemoved:: just for testing generating in PDF
    			'animation' => false, //ToBeRemoved:: just for testing generating in PDF
    			'shadow' => false,
    			//'lineWidth' =>  4,
    			//'stroke-width' => 22,
    	));
    	$ob1->plotOptions->series(array(
    			'enableMouseTracking' => false, //ToBeRemoved:: just for testing generating in PDF
    			'animation' => false, //ToBeRemoved:: just for testing generating in PDF
    			'shadow' => false,
    	));
    	$ob->exporting->buttons(array('enabled' => true));
    	$dataChart = array(
    			array($translator->trans('Quotes'), $data['totalQuote']),
    			array($translator->trans('Confirmed'), $data['totalConfirmed']),
    	
    			);
    	$ob1->series(array(array('type' => 'pie','name' => $translator->trans('Num of Quotes'), 'data' => $dataChart)));
    	 
    	
    	$charts = array();
    	$charts[] = $ob;
    	$charts[] = $ob1;
    	 
    	return $charts;
    }
    
    /**
     * @Route("/test-pdf-generate", name="qmxDashboard_report_test_pdf")
     * @Template("QuotemaxDashboardBundle:Report:view.html.twig")
     */
    public function testPdfAction(Request $request)
    {
    	//ToFindOut:
    	//	- https://code.google.com/p/wkhtmltopdf/issues/detail?id=835
    	//	- https://code.google.com/p/wkhtmltopdf/issues/detail?id=689
    	//	- http://api.highcharts.com/highcharts#plotOptions.line
    	//	- http://stackoverflow.com/questions/12621327/highcharts-display-in-my-render-html-page-which-i-want-to-convert-into-pdf-using
    	//  - http://madalgo.au.dk/~jakobt/wkhtmltoxdoc/wkhtmltopdf_0.10.0_rc2-doc.html
    	//  - http://stackoverflow.com/questions/23650539/wkhtmltopdf-knp-snappy-custom-height-width-which-unit-is-expected
    	//  - http://stackoverflow.com/questions/8995177/render-highcharts-canvas-as-a-png-on-the-page 
    	//	- http://forum.highcharts.com/viewtopic.php?f=9&t=16179
    	//  - http://www.claytonlz.com/2011/02/broken-highcharts-wkhtmltopdf/
    	//  - https://code.google.com/p/wkhtmltopdf/issues/detail?id=1266
    	//  - https://github.com/flot/flot
    	//  - https://code.google.com/p/wkhtmltopdf/downloads/list?can=1&q=&colspec=Filename+Summary+Uploaded+ReleaseDate+Size+DownloadCount
    		
    	$this->forSuperAdminOnly();
    	 
    	//translator
    	$translator = $this->get('translator');
    	 
    	 
    	//form
    	$today =  new \DateTime();
    	$fromDate =  $today->modify('-6 month');
    	$toDate =  new \DateTime();
    	$form = $this->createFormBuilder()
    	->add('From', 'date', array('widget' => 'single_text', 'format' => 'yyyy-MM-dd',
    			'attr' => array('class' => 'datepicker'),
    			'data' => $fromDate
    	))
    	->add('To', 'date', array('widget' => 'single_text',  'format' => 'yyyy-MM-dd',
    			'attr' => array('class' => 'datepicker'),
    			'data' => $toDate
    	))
    	->add('Query', 'submit')
    	->getForm();
    	 
    	$form->handleRequest($request);
    	 
    	$data = null;
    	$options = array();
    	 
    	$data = $this->getQuoteRepository()->getReportData($form, $options);
    	 
    	/*
    	 if ($form->isValid()) {
    		
    	$data = $this->getQuoteRepository()->getReportData($form, $options);
    
    	}else{
    	$data = $this->getQuoteRepository()->getReportData($form, $options);
    	}
    	*/
    	 
    	//render charts
    	$charts = $this->renderCharts($data);
    	
    	//ToDo: disable mouseAnimation for generating in PDF
    	//$charts[0]->plotOptions->enableMouseTracking = false;
    	//$charts[1]->plotOptions->enableMouseTracking = false;
    	//var_dump($charts[0]->plotOptions);
    	
    	if(empty($data['totalQuote'])){
    		$conversionRate  = 0;
    	}else{
    		$conversionRate = floatval($data['totalConfirmed'] /  $data['totalQuote']) * 100;
    	}
    	

    	//render a pdf document as response from a controller
    	$html = $this->renderView('QuotemaxDashboardBundle:Report:testPdf.html.twig', array('chart' => $charts[0],
    			'chart1' => $charts[1],
    			'conversionRate' => $conversionRate,
    			'form' => $form->createView(),
    	));
    	$filename = 'testPdfGeneratedWithHighChart.pdf';
    	
    	//output to page
    	//return new Response($html);
    	
    	//output to pdf
    	return new Response(
    			$this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
    				//'redirect-delay' => 3000
    				'javascript-delay' => 10000,
    				//'enable-javascript' => true
    			)),
    			200,
    			array(
    					'Content-Type'          => 'application/pdf',
    					'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
    			)
    	);
    	
    	
    	
    	
    	/*
    	 
    	return array('chart' => $charts[0],
    			'chart1' => $charts[1],
    			'conversionRate' => $conversionRate,
    			'form' => $form->createView(),
    	);*/
    	
    	
    	/*
    	  $pdfString=$this->knp_snappy->getOutputFromHtml($html, array(
                'orientation' => 'landscape', 
                'enable-javascript' => true, 
                'javascript-delay' => 1000, 
                'no-stop-slow-scripts' => true, 
                'no-background' => false, 
                'lowquality' => false,
                'page-height' => 600,
                'page-width'  => 1000,
                'encoding' => 'utf-8',
                'images' => true,
                'cookie' => array(),
                'dpi' => 300,
                'image-dpi' => 300,
                'enable-external-links' => true,
                'enable-internal-links' => true
            ));
    	 
    	 
    	 */
    }
    
    
    
  
}