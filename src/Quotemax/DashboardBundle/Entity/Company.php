<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\CompanyRepository")
 * @ORM\Table(name="company")
 */
class Company
{
	/*	TODO:
	 * 		- add created_at + modifited_at as trailclass
	 * 
	 */
	
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Quotemax\DashboardBundle\Entity\UserDetail", mappedBy="company",
     * 					cascade={"persist", "remove"})
     */
    protected $users;
    
    
    /**
     * @ORM\Column(type="string", length=20, unique=true, nullable=false)
     *
     * **NOTE: let manually set this code by admin only when creating new company
     */
    protected $clientCode;
    
    
    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    protected $name;
    
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     */
    protected $type;
       

    /**
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * 
     */
    protected $billingAddress;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $isSameAsBilling;
    
    
    /**
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * 
     */
    protected $shippingAddress;
    
     
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $contactName;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $phoneNumber;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $faxNumber;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $mobileNumber;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     *	//probably use for log-in for user
     */
    protected $email;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $website;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $instantMessaging;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $unit;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $currency;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     */
    protected $discount = 5;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     */
    protected $adminFeeEur = 10;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     */
    protected $adminFeeUsd = 15;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $homeMessageEn;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $homeMessageFr;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $transportRemoteAreaSurcharge = true;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $transportFuelSurcharge = true;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $transportSafetySurcharge = true;
    
    
    /**
     * @ORM\OneToOne(targetEntity="Quotemax\DashboardBundle\Entity\CompanyDetail",
     * 						mappedBy="company", cascade={"persist", "remove", "merge"})
     *
     */
    protected $detail;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $internal = false;
    
    
    

    public function __construct()
    {
    	$this->users = new ArrayCollection();
    }
    
 
    public function __toString(){
    	
    	$name = $this->name;
    	if($this->shippingAddress){
    		//$name = $name." (in ".$this->shippingAddress->getCity().", id: ".$this->id.")";
    		$subName = "in ".$this->shippingAddress->getCity();
    		if(count($this->getUsers())){
    			$subName = " (".$subName.", id: ".$this->getUsers()[0]->getUser()->getId().")";
    		}
    		$name = $name."".$subName;
    	}
    	return $name;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

 

    /**
     * Set clientCode
     *
     * @param string $clientCode
     * @return Company
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;

        return $this;
    }

    /**
     * Get clientCode
     *
     * @return string 
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Company
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isSameAsBilling
     *
     * @param boolean $isSameAsBilling
     * @return Company
     */
    public function setIsSameAsBilling($isSameAsBilling)
    {
        $this->isSameAsBilling = $isSameAsBilling;

        return $this;
    }

    /**
     * Get isSameAsBilling
     *
     * @return boolean 
     */
    public function getIsSameAsBilling()
    {
        return $this->isSameAsBilling;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     * @return Company
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string 
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Company
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set faxNumber
     *
     * @param string $faxNumber
     * @return Company
     */
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get faxNumber
     *
     * @return string 
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     * @return Company
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string 
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Company
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set instantMessaging
     *
     * @param string $instantMessaging
     * @return Company
     */
    public function setInstantMessaging($instantMessaging)
    {
        $this->instantMessaging = $instantMessaging;

        return $this;
    }

    /**
     * Get instantMessaging
     *
     * @return string 
     */
    public function getInstantMessaging()
    {
        return $this->instantMessaging;
    }



    /**
     * Set currency
     *
     * @param string $currency
     * @return Company
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return Company
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set adminFeeEur
     *
     * @param string $adminFeeEur
     * @return Company
     */
    public function setAdminFeeEur($adminFeeEur)
    {
        $this->adminFeeEur = $adminFeeEur;

        return $this;
    }

    /**
     * Get adminFeeEur
     *
     * @return string 
     */
    public function getAdminFeeEur()
    {
        return $this->adminFeeEur;
    }

    /**
     * Set adminFeeUsd
     *
     * @param string $adminFeeUsd
     * @return Company
     */
    public function setAdminFeeUsd($adminFeeUsd)
    {
        $this->adminFeeUsd = $adminFeeUsd;

        return $this;
    }

    /**
     * Get adminFeeUsd
     *
     * @return string 
     */
    public function getAdminFeeUsd()
    {
        return $this->adminFeeUsd;
    }

    /**
     * Set homeMessageEn
     *
     * @param string $homeMessageEn
     * @return Company
     */
    public function setHomeMessageEn($homeMessageEn)
    {
        $this->homeMessageEn = $homeMessageEn;

        return $this;
    }

    /**
     * Get homeMessageEn
     *
     * @return string 
     */
    public function getHomeMessageEn()
    {
        return $this->homeMessageEn;
    }

    /**
     * Set homeMessageFr
     *
     * @param string $homeMessageFr
     * @return Company
     */
    public function setHomeMessageFr($homeMessageFr)
    {
        $this->homeMessageFr = $homeMessageFr;

        return $this;
    }

    /**
     * Get homeMessageFr
     *
     * @return string 
     */
    public function getHomeMessageFr()
    {
        return $this->homeMessageFr;
    }

    /**
     * Set billingAddress
     *
     * @param \Quotemax\DashboardBundle\Entity\Location $billingAddress
     * @return Company
     */
    public function setBillingAddress(\Quotemax\DashboardBundle\Entity\Location $billingAddress = null)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return \Quotemax\DashboardBundle\Entity\Location 
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set shippingAddress
     *
     * @param \Quotemax\DashboardBundle\Entity\Location $shippingAddress
     * @return Company
     */
    public function setShippingAddress(\Quotemax\DashboardBundle\Entity\Location $shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress
     *
     * @return \Quotemax\DashboardBundle\Entity\Location 
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set unit
     *
     * @param string $unit
     * @return Company
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Add users
     *
     * @param \Quotemax\DashboardBundle\Entity\UserDetail $users
     * @return Company
     */
    public function addUser(\Quotemax\DashboardBundle\Entity\UserDetail $users)
    {
    	$users->setCompany($this);
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Quotemax\DashboardBundle\Entity\UserDetail $users
     */
    public function removeUser(\Quotemax\DashboardBundle\Entity\UserDetail $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set transportRemoteAreaSurcharge
     *
     * @param boolean $transportRemoteAreaSurcharge
     * @return Company
     */
    public function setTransportRemoteAreaSurcharge($transportRemoteAreaSurcharge)
    {
        $this->transportRemoteAreaSurcharge = $transportRemoteAreaSurcharge;

        return $this;
    }

    /**
     * Get transportRemoteAreaSurcharge
     *
     * @return boolean 
     */
    public function getTransportRemoteAreaSurcharge()
    {
        return $this->transportRemoteAreaSurcharge;
    }

    /**
     * Set transportFuelSurcharge
     *
     * @param boolean $transportFuelSurcharge
     * @return Company
     */
    public function setTransportFuelSurcharge($transportFuelSurcharge)
    {
        $this->transportFuelSurcharge = $transportFuelSurcharge;

        return $this;
    }

    /**
     * Get transportFuelSurcharge
     *
     * @return boolean 
     */
    public function getTransportFuelSurcharge()
    {
        return $this->transportFuelSurcharge;
    }

    /**
     * Set transportSafetySurcharge
     *
     * @param boolean $transportSafetySurcharge
     * @return Company
     */
    public function setTransportSafetySurcharge($transportSafetySurcharge)
    {
        $this->transportSafetySurcharge = $transportSafetySurcharge;

        return $this;
    }

    /**
     * Get transportSafetySurcharge
     *
     * @return boolean 
     */
    public function getTransportSafetySurcharge()
    {
        return $this->transportSafetySurcharge;
    }
    
    
    /**
     * Set internal
     *
     * @param boolean $internal
     * @return Company
     */
    public function setInternal($internal)
    {
    	$this->internal = $internal;
    
    	return $this;
    }
    
    /**
     * Get internal
     *
     * @return boolean
     */
    public function getInternal()
    {
    	return $this->internal;
    }
    

    /**
     * Set detail
     *
     * @param \Quotemax\DashboardBundle\Entity\CompanyDetail $detail
     * @return Company
     */
    public function setDetail(\Quotemax\DashboardBundle\Entity\CompanyDetail $detail = null)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return \Quotemax\DashboardBundle\Entity\CompanyDetail 
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    
}
