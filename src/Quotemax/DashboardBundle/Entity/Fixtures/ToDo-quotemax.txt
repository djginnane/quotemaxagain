ToNOte:::L
	- FTP path on RHCloud hosting >> '/var/lib/openshift/541174315973cabaed00052a'
	- http://moquet.net/blog/5-features-about-composer-php/ << Be ready for production
	- http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-typography.php  << text formatting on Bootstrap3
	
Database: quotemax User: adminIJUAY8P Password: 91sIcHq5Mu84

ToSendEmail:::
php app/console swiftmailer:spool:send --time-limit=10 --env=prod
 

ToDo:
2015-02
	- [] to add this command into composer.json
		php app/console cache:clear --env=prod --no-debug
		php app/console assets:install web_directory
		php app/console assetic:dump web_directory
	- set cronjob for spooling email
		> https://forums.openshift.com/how-to-execute-python-script-from-a-cron-job-via-bash
		> http://www.openshift.org/documentation/oo_user_guide.html#cron
		
ToSpoolEmail:
	- http://knpuniversity.com/screencast/question-answer-day/swiftmailer-spooling
	- http://stackoverflow.com/questions/14426152/swift-mailer-and-symfony2-no-mail-in-the-spool
	- http://symfony.com/doc/2.5/cookbook/email/spool.html
	- https://groups.google.com/forum/#!topic/swiftmailer/nKfVhIHqVkw
	- http://stackoverflow.com/questions/27323662/symfony2-send-email-warning-mkdir-no-such-file-or-directory-in
	
	
	mkdir -p bin/wkhtmltopdf
chmod 777 bin/wkhtmltopdf


2014-08-14 Thur | 2014-08-22 Friday | 2014-08-25 Monday:
	- [DONE]'Acrylic' product, 'Illuminated Area' dropdown will effect to hide/display 'Options (Front)/ Options (Return)' (@Yann, can you provide which options in dropdown?)
	- [DONE] insert line separator at each section at both email & pdf
	- [DONE] global setting for 3 types of transport surcharges
	- [DONE] add 'requirement' field (additional remark) + pop-up tooltip(?) to help client to add specific notes & place it before documents field
	- [DONE] auto-check for 'letters' & its 'repeat' fields
	- [DONE, pending for french] change confirmation text at quote creation (@Yann, can you provide both 'en' & 'fr' text?)
	- [DONE] add link to company profile at quote table for admin only
	- [DONE] auto-update 'pending' to 'expired' status after 30 days
        - [DONE] add the expiry reminder text at footer of 'Confirmation' page (@Yann, can you provide both 'en' & 'fr' text?)
	- [DONE] fix bug @Acrynox when option 'none'
	- [DONE] change label of measure unit 'Metric (mm./cm.)' & 'Inches'
	- [DONE] 'Plaque' product selected, hide 'Pattern type', 'Letters', 'Repeat', 'Depth' fields from input forms, pdf & email
	- [DONE] change text at top-main menu ['Quote' to 'Quotation', 'Order Confirm' to 'Validation', 'Orders' to 'Follow-Up']
	- [DONE] about cosmetic / layout design for white background , (to find some free bootstrap theme)
	- [DONE] about 'fr' translations, (@Yann, can you provide 'fr' translations  for those 3 files in CSV & XML which we sent previously) 
		- [DONE] email template
	- [DONE] add setting for 'Quotation Validity Period' at Admin menu (auto-update 'pending' to 'expired')
	- [DONE, for client to validate]send email when client clicks 'confirm' button to both client & admin + attach invoice generated in PDF (@Yann, can you please provide email template (ex. include invoice info + payment bank detail) in both 'en' & 'fr')
		- [DONE] send another email to admin only to confirm order from client
		> [DONE, QToAsk] 'Due Date' come from today + numOfDeliveryDatys?
	- [DONE] add new field, 'payment status' ['yes','no'] for Admin only to click confirm payment from client at quote table (column name 'AR.' or Account Receivable)
	- [DONE] send email to confirm both client & admin when 'payment status' updated from 'no' to 'yes'
	- [DONE] add setting for 5 'Product' at Admin menu
		- [DONE] raw materials rates
		- [DONE]coefficient for both Led Face & Return
	- [DONE] add new 'Battery' cost into 3 products' calculation(@Yann, can you please provide new-updated Pricing Calculation in Excel?)
	- [OPTIONAL] graphical reporting + filter by creation date
		- switch to jquery datepicker
	- [DONE] complete quote form
		- [DONE] fix form when switch to 'fr', probably add slug
		- [SKIP] fix checkbox-all @ orderConfirmation
		- [DONE] measureUnit for 'inch' case, convert into cm. before submit
		- [DONE] logic to remove generated PDF after sending email
	- [DONE] add setting for bcc email
	- [] filter upload file type + limit max size
	- [DONE] display requirment text everywhere
	- [] check if admin fee + discount must obtain from company profile setting, not from token!!
	- [DONE] remove signinox & signique from dropdown at quote creation
	- [DONE] fix French character not supported in generated PDF
	- [] check/fix for 'Plague' product for depth as required for volumne for transport price

ToNoteAboutAdapterCost:
	- adapter, inox using only 'return' calculation
	- adapter, acrylic will effect due to option whether 'face' or 'return' calculation
	- adapter calculation for INox optional
	- adapter, acroynox only using 'return' calculation
	- put in one excel & send to Yann >>> about 'fr' translations, (@Yann, can you provide 'fr' translations  for those 3 files in CSV & XML which we sent previously) 
	- website of icon links http://fortawesome.github.io/Font-Awesome/icons/
 'Illuminated Area' dropdown will effect to hide/display 'Options (Front)/ Options (Return)'



2014-08-05 - 06 - 07 - 13 :
	- [DONE] email system:
		- [PENDING] when change new email/login to validate
	- [] translation
		- use gedmo translatable
		- [DOING] ask translation for product & options 
	
	- [DONE] Navgation bar + quote-detail preview before acutal submit
	- [DOING] graphical reporting + filter by creation date
		- switch to jquery datepicker
	- [DONE] add configure global messaging at admin
	- [DONE] add configure currency at admin
	- [DONE] complete user form
		- [DONE] generate client code
		- [DONE] internal feedbacks
		- [DONE] when delete user, just mark status 'inactive' (not actaul deleting from DB)
	- [] complete quote form
		- [] fix form when switch to 'fr', probably add slug
		- fix email template to complatible with mobile view
		- fix checkbox-all @ orderConfirmation
		- [DONE] remove code input + use clientCode from profile
		- measureUnit for 'inch' case, convert into cm. before submit
		- logic to remove generated PDF after sending email
		- [DONE] remove company dropdown for non-admin user

2014-08-04 Mon:
	- email system:
		- new user
	- [DOING] translation
		- use gedmo translatable
		- [DOING] ask translation for product & options 
	
	- Navgation bar + quote-detail preview before acutal submit
	- [DONE] transport rate at admin
		- [DONE] create standard loading template in CSV
		- [DONE] able to config for other transport rates options ['remoteAreaSurcharge', 'FuelSurcharge', 'SafetySurcharge'] 
	- graphical reporting + filter by creation date
	- add configure global messaging at admin
	- add configure currency at admin
	- complete user form
		- [DONE] add another 2 options at admin ['Remote Area Surcharge', 'Safety Surcharge']
		- allow numerice
		- when delete user, just mark status 'inactive' (not actaul deleting from DB)
	- complete quote form
		- remove code input + use clientCode from profile
		- measureUnit for 'inch' case, convert into cm. before submit
		- remove company dropdown for non-admin user
		


2014-07-28 - 31:
	- email system:
		- [DONE] quote + attach generated PDF
		- new user
	- translation
		- use gedmo translatable
		- ask translation for product & options 
	- [DOING] compelete pricing engine
		- [DONE] related rule	
		- [DONE] create dynamic token variables table
		- [DONE] debug mode for pricing calculation
		- [DONE] configure all 5 products
			-> [DONE] modify form accordingly (show surface only for 'Acrylic'
				- [DONE] cutting service fee for 'Flat Cut' & 'Plague'
				- [DONE] Acrynox, 'Return Sticker 3M ScotchCal' ((Depth-Return inox)*Perimeter)*Rate
		Quesion:
		  Acrylic:
			- OK
		  INOX:
			- [HOW TO DO??] about 'Additional/Optional Costs', when it will be triggered to calculated this rules['Primatic Face +50% of Total Cost', 'Serif +10% of Total Cost']?  Should we integrate this into the INOX calcuation or just ignore this rule? 
		
	- Navgation preview before actual preview
	- [ToDoNext] transport rate at admin
		- create standard loading template in CSV
		- able to config for other transport rates options ['remoteAreaSurcharge', 'FuelSurcharge', 'SafetySurcharge'] 
	- graphical reporting + filter by creation date
	- global messaging at admin
	- add configure currency at admin
	- complete user form
		- add another 2 transport options
	- complete quote form

Setting Up Server for Symfony Application::::
	- http://ziad.eljammal.com/intro-symfony-2/

Minimum Requirement for setting up Test/Demo Environment server for PHP Web App projects
	- OS: Window Server 2008 (or higher) , Linux Ubuntu (okay with me if there is graphical interface mode for me to remote log-in)
	- CPU: Processor Quad (4) core 
	- Memory RAM: 8GB (at least)
	- Harddisk:	100GB 
	- bandwidth: low traffic for both inter & domestic (just for client for now)
	- Domain:	demo-quotemax.ascentec.net (just proposing) 

2014-07-28 Mon:
	- email system:
		- [DONE] quote + attachment
		- new user
	- translation
		- use gedmo translatable
		- ask translation for product & options 
	- compelete pricing engine
		- related rule
		- configure all 5 products
	- transport rate at admin
	- graphical reporting + filter by creation date
	- global messaging at admin
	- add configure currency at admin
	- complete user form
	- complete quote form

2014-07-14 Mon:
	- [] design schema database
	- [DOING] user management
		- [] create new 'company' / 'admin' user
		- [] update user profile
		- [] notification message (company-level)
		- [DONE] log-in
	- [] locales switching ['en', 'fr']
		- [] set up translation engine
		- [] tranlate list of labels in 'fr'
		- [] testing - user modules & locales switching




2014-07-10 Thur:
	- [DONE] setup layout & main templates
		- [DONE] create main layout.html.twig
			> http://bootstrap.braincrafted.com/components.html#flash
		- [DONE] create main menu (at top)


2014-07-09 Wed:
	- [DOING] setup layout & main templates
		- [TOCONTINUE, DOING] create main layout.html.twig
			> http://bootstrap.braincrafted.com/components.html#flash
		- [DOING] create main menu (at top)
	
	- to include third-party bundles:
		- Use Twitter Bootstrap in Symfony2 with Composer, https://coderwall.com/p/kzyiaw
		- Easy integration of twitters bootstrap into symfony2, https://github.com/phiamo/MopaBootstrapBundle
		- http://www.ggkf.com/node-js/compiling-bootstrap-3-in-a-symfony-2-project-on-windows
		- BraincraftedBootstrapBundle integrates Bootstrap into Symfony2 by providing templates, Twig extensions, services and commands.,
			http://bootstrap.braincrafted.com/getting-started.html


2014-07-08 Tue:
	- [DONE] setup symfony
	- [DONE] git
	- [DONE] clone quoteMax
	- [DONE] create user bundle & security
		- https://github.com/FriendsOfSymfony/FOSUserBundle/blob/master/Resources/doc/index.md
		- http://stackoverflow.com/questions/19751181/fosuserbundle-no-route-found-for-get-login
		- http://stackoverflow.com/questions/16279378/symfony2-fosuserbundle-route-issues-with-multiple-bundles
	
rch
WD 1TB, 208