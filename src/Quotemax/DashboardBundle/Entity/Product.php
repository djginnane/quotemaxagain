<?php
namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Product
{
	/*
	 * **To see a list of all available field types and more information about setting options & behaviors, see Doctrine's Mapping Types documentation
	 * 	- http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html#property-mapping 
	 */
	
	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	protected $id;
	
	
	/**
	 * @ORM\Column(type="string", length=100)
	 * 
	 */
	protected $name;


	/**
	 * @ORM\Column(type="text", nullable=true)
	 * 
	 */
	protected $description;
	
	
	/**
	 * @ORM\OneToMany(targetEntity="ItemOption", mappedBy="product", cascade={"persist", "remove"})
	 *
	 */
	protected $itemOptions;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="Company")
	 *
	 */
	protected $company;
	
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->itemOptions = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	
	public function __toString(){
		return $this->name;
	}
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }




    /**
     * Add itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     * @return Product
     */
    public function addItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
        $this->itemOptions[] = $itemOptions;

        return $this;
    }

    /**
     * Remove itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     */
    public function removeItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
        $this->itemOptions->removeElement($itemOptions);
    }

    /**
     * Get itemOptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemOptions()
    {
        return $this->itemOptions;
    }

    /**
     * Set company
     *
     * @param \Quotemax\DashboardBundle\Entity\Company $company
     * @return Product
     */
    public function setCompany(\Quotemax\DashboardBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Quotemax\DashboardBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
