<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Quotemax\DashboardBundle\Entity\Traits\TimestampTrait;
use Quotemax\DashboardBundle\Entity\Item;
use Quotemax\DashboardBundle\Entity\TransportRate;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\QuoteRepository")
 * @ORM\Table(name="quote")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class Quote
{
	//use TimestampTrait;
	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", length=100, unique=true, nullable=false)
	 *
	 */
	protected $code;
	
	
    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     *
     */
    protected $name;
    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Company")
     *
     */
    protected $company;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Quotemax\UserBundle\Entity\User")
     *
     */
    protected $user;
    
    /**
     * @ORM\OneToOne(targetEntity="Quotemax\DashboardBundle\Entity\QuoteDetail",
     * 						mappedBy="quote", cascade={"persist", "remove", "merge"})
     *
     */
    protected $detail;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $description;
    
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     */
    protected $quantity;
    
    
    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     *
     */
    protected $currency = 'eur';
    
    
    /**
     * @ORM\Column(type="decimal", scale=5, nullable=false)
     *
     */
    protected $currencyRate;
    
    
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     *
     */
    protected $measureUnit;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     *
     */
    protected $status;
    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $trackingNo;
    
    
    /**
     * @ORM\Column(type="boolean", nullable=false)
     *
     */
    protected $statusPayment = false;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $modifiedAt;
    
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     * 
     */
    private $confirmedAt;
    
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $paidAt;
    
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $deliveredAt;
    

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * $pricePerPattern(in THB) =  price of all elements in this patterns, including:
     * 								- 'raw materials'
     * 								- 'options selected'
     * 								- 'labor cost'
     */
    //protected $pricePerPattern;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     * 
     * **Total Cost (per pattern)
     * 
     */
    protected $price;    
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **Margin = Total Cost * 0.2; (per pattern)
     */
    protected $margin = 0;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **additional cost w/o margin (per pattern)
     *
     */
    protected $additionalCost = 0;
    
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **totalSellingPrice = totalCost + margin + additionalCost (per pattern)
     */
    protected $priceSelling = 0;
    
    

    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **totalSellingPriceAll = totalSellingPrice * quantity
     */
    protected $priceSellingTotalAll = 0;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * **discount = $priceSellingTotalAll * discount(%) 
     */
    protected $discount = 0;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * ** adminFee = (EUR = 10, USD = 15) * exchangeRate (toTHB)
     */
    protected $adminFee = 0;
    
    
    /** 
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     * 
     * 
     * $priceTotal(in THB) = ($price * (1 - $discount/100)) + $adminFee, including:
     * 							- deduct for $discount(5%)
     * 							- $adminFee(in THB) (ex. 'EUR' for 10 & 'USD' 15)
     * 
     * note: this is total price of this quote excluded transport price
     * 
     * **totalSellingPriceFinal = totalSellingPriceAll - discount + adminFee
     * 
     */
    protected $priceSellingTotalFinal;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * ** priceGrandTotal = priceSellingTotalFinal + priceTransport
     */
    protected $priceGrandTotal = 0;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * ** totalVolume of all items in quote (in cubic cm.)
     */
    protected $volume = 0;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * ** weightTransport (in kg.) = (totalVolume * %coefficient-volume-transport% (3.0)) / %volume-to-weight-dhl% (5000.0)
     */
    protected $weightTransport = 0;
     
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     * 
     * $priceTransport (in THB) = $weightTransport to be looking up ( at pricing-rate matrix of DHL) + all 3 types of surcharges
     * 
     */
    protected $priceTransport;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * $priceTransport (in THB) = $weightTransport to be looking up ( at pricing-rate matrix of DHL)
     *
     */
    protected $priceTransportSub;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     *
     */
    protected $priceTransportRemoteArea;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     *
     */
    protected $priceTransportFuel;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     *
     */
    protected $priceTransportSafety;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     *
     */
    protected $numOfLed;
    
    
    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     *
     */
    protected $priceAdaptor;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     *
     */
    protected $adaptorType;
    
    

    
    
    public function __construct(){
    	
    	$this->setDetail(new QuoteDetail());
    	//$this->setCode($this->random_string(8));
    	$this->setStatus('PEN'); // status => ['CON', 'PEN', 'EXP']
    	$this->setQuantity(1);
    	
    	$this->setConfirmedAt( new \Datetime("0000-00-00 00:00:00"));
    	$this->setPaidAt( new \Datetime("0000-00-00 00:00:00"));
    	$this->setDeliveredAt( new \Datetime("0000-00-00 00:00:00"));
    	
    	
    }
    
    //utility functions
    function random_string($length) {
    	$key = '';
    	$keys = array_merge(range(0, 9), range('a', 'z'));
    
    	for ($i = 0; $i < $length; $i++) {
    		$key .= $keys[array_rand($keys)];
    	}
    
    	return $key;
    }

    public function calculatePrice($tokensDb = array(), $transportRates = array(),  &$auditLog = ""){
    	
    	$price = 0;
    	$margin = 0;
    	$additionalCost = 0;
    	$priceSelling = 0; // $priceSelling = ($price + $margin + $additionalCostPP)
    	$priceSellingTotalAll = 0; //  $priceSellingTotalAll = $priceSelling * $quantity
    	$discount = 0;
    	$adminFee = 0;
    	$priceSellingTotalFinal = 0;
    	$volume = 0;
    	$weightTransport = 0;
    	$priceGrandTotal = 0;
    	
		//find any extra tokens before looping to calculate each item
    	$tokensDb = $this->getTokens($tokensDb);
    	
    	//find price per one pattern set (for both all letters + graphic logos)
    	if($this->getDetail()){
    		if($this->getDetail()->getItems()){
    			$items = $this->getDetail()->getItems();
    			
    			/* @var $item Item */
    			foreach($items as $item){
    				
    				$auditLogItem = "";
    				$item->calculatePrice($tokensDb, $auditLogItem);
    				
    				
    				$price += $item->getPrice();
    				$margin +=  $item->getMargin();
    				$additionalCost += $item->getAdditionalCost();
    				$priceSelling += $item->getPriceSelling();
    				$priceSellingTotalAll += $item->getPriceSellingTotalAll();
    				
    				$volume += $item->getVolume();
    				
    				$auditLogItem .= "<li>TotalCost: ".number_format($item->getPrice(),2)."</li>";
    				$auditLogItem .= "<li>Margin (".($tokensDb['%margin%'] * 100)."%): ".number_format($item->getMargin(),2)."</li>";
    				$auditLogItem .= "<li>AdditonalCost: ".number_format($item->getAdditionalCost(),2)."</li>";
    				$auditLogItem .= "<li>Volume: ".number_format($item->getVolume(),2)."</li>";
    				$auditLogItem .= "<li>SellingPricePerItem: ".number_format($item->getPriceSelling(),2)."</li>";
    				$auditLogItem .= "<li>Quantity: ".$item->getQuantity()."</li>";
    				$auditLogItem .= "<li><span class='label label-warning txt-totalPrice'>TotalPrice: ".number_format($item->getPriceSellingTotalAll(),2)."</span></li>";
    				
    				$auditLog .= "<ul id='item-".$item->getId()."'><li><b>Item: ".$item->getName()."</b></li>".$auditLogItem."</ul>";
    			}
    		}
    	}
    	
    	//find discount
    	$discount = $priceSellingTotalAll * $tokensDb['%discount%'];
    	
    	//find adminFee in THB
    	$adminFee = $tokensDb['%admin-fee-'.$this->getCurrency().'%'] * $tokensDb['%'.$this->getCurrency().'thb%'];
    	
    	
    	//find total final selling price 
    	$priceSellingTotalFinal = $priceSellingTotalAll - $discount + $adminFee;
    	
    	
    
    	//set properties
    	$this->setPrice($price);
    	$this->setMargin($margin);
    	$this->setAdditionalCost($additionalCost);
    	$this->setPriceSelling($priceSelling);
    	$this->setPriceSellingTotalAll($priceSellingTotalAll);
    	$this->setDiscount($discount);
    	$this->setAdminFee($adminFee);
    	$this->setPriceSellingTotalFinal($priceSellingTotalFinal);
    	
    	//calculate adaptor cost
    	$this->calculatePriceAdaptor($tokensDb, $auditLog);
    	
    	//calculate transport cost
		$this->setVolume($volume);
		$weightTransport = ($this->getVolumeTotal() * $tokensDb['%coefficient-volume-transport%']) / $tokensDb['%volume-to-weight-dhl%'];
		$this->setWeightTransport($weightTransport);
    	$this->calculatePriceTransport($tokensDb, $transportRates, $auditLog);
    	
    	
    	//sum up grand total 
    	$priceGrandTotal = $priceSellingTotalFinal + $this->getPriceTransport() + $this->getPriceAdaptor();
    	$this->setPriceGrandTotal($priceGrandTotal);
    	
    	
   
   		//set audit log
   		//quote product price 	
    	$auditLogQ  = "";
    	$auditLogQ .= "<li><b>Quotation #: ".$this->getCode()."</b></li>";
    	$auditLogQ .= "<li>TotalCost (PerPattern): ".number_format($this->getPrice(),2)."</li>";
    	$auditLogQ .= "<li>TotalMargin (PerPattern): ".number_format($this->getMargin(),2)."</li>";
    	$auditLogQ .= "<li>TotalAdditionalCost (PerPattern): ".number_format($this->getAdditionalCost(),2)."</li>";
    	$auditLogQ .= "<li>TotalPriceSelling (PerPattern): ".number_format($this->getPriceSelling(),2)."</li>";
    	$auditLogQ .= "<li>TotalQuantity: ".$this->getQuantity()."</li>";
    	$auditLogQ .= "<li>TotalPriceSellingAll: ".number_format($this->getPriceSellingTotalAll(),2)."</li>";
    	$auditLogQ .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Discount (".($tokensDb['%discount%'] * 100)."%): ".number_format($this->getDiscount(),2)."</li>";
    	$auditLogQ .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AdminFee (".$tokensDb['%admin-fee-'.$this->getCurrency().'%'].".0 ".$this->getCurrency()." ): ".number_format($this->getAdminFee(),2)."</li>";
    	$auditLogQ .= "<li>TotalFinalPriceSelling (incl -discount, +adminFee): ".number_format($this->getPriceSellingTotalFinal(),2)."</li>";
    	$auditLogQ .= "<li>Exchange Rate: ".$this->getCurrencyRate()." (to ".strtoupper($this->getCurrency())." )"."</li>";
    	$auditLogQ .= "<li><span class='label label-warning txt-totalPrice'>TotalFinalPriceSelling (in ".strtoupper($this->getCurrency())."): ".$this->getPriceSellingTotalFinalCC()."</span></li>";
    	
    	
    	//adaptor price
    	if($this->getNumOfLed() > 0){
	    	$auditLogQ .= "<li>TotalNumOfLeds: ".$this->getNumOfLed()."</li>";
	    	$auditLogQ .= "<li>TotalAdapterTypes: ".$this->getAdaptorType()."</li>";
	    	$auditLogQ .= "<li>TotalPriceAdapter: ".number_format($this->getPriceAdaptor(),2)."</li>";
	    	$auditLogQ .= "<li><span class='label label-warning txt-totalPrice'>TotalPriceAdapter: (in ".strtoupper($this->getCurrency())."): ".$this->getPriceAdaptorCC()."</span></li>";
    	}
    	
    	//transport price
    	$auditLogQ .= "<li>TotalVolume: ".number_format($this->getVolumeTotal(),2)."</li>";
    	$auditLogQ .= "<li>TotalWeightTransport: ".number_format($this->getWeightTransport(),2)."</li>";
    	$auditLogQ .= "<li>Sub-TotalPriceTransport: ".number_format($this->getPriceTransportSub(),2)."</li>";
    	$auditLogQ .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remote Area Surcharge (".($tokensDb['%transport-remote-area-surcharge%'] * 100)."%) :".number_format($this->getPriceTransportRemoteArea(),2)."</li>";
    	$auditLogQ .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fuel Surcharge (".($tokensDb['%transport-fuel-surcharge%'] * 100)."%) :".number_format($this->getPriceTransportFuel(),2)."</li>";
    	$auditLogQ .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Safety Surcharge (".($tokensDb['%transport-safety-surcharge%'] * 100)."%) :".number_format($this->getPriceTransportSafety(),2)."</li>";
    	$auditLogQ .= "<li>TotalPriceTransport: ".number_format($this->getPriceTransport(),2)."</li>";
    	$auditLogQ .= "<li><span class='label label-warning txt-totalPrice'>TotalPriceTransport: (in ".strtoupper($this->getCurrency())."): ".$this->getPriceTransportCC()."</span></li>";
    	
    	
    	$auditLogQ .= "<li>GrandTotal: ".number_format($this->getPriceGrandTotal(),2)."</li>";
    	$auditLogQ .= "<li><span class='label label-warning txt-totalPrice'>GrandTotal: (in ".strtoupper($this->getCurrency())."): ".$this->getPriceGrandTotalCC()."</span></li>";
    	
    	$auditLog = "<ul>".$auditLogQ."</ul>".$auditLog;
    	
    	
    	
    	return $priceSellingTotalFinal;
    }
    
    public function calculatePriceAdaptor($tokensDb = array(), &$auditLog = ""){
    	//TODO
    	//
    	
    	//check if the quote require adaptor calculation or not
    	//TOCHECK: probably not neccessary check for this
    	/*if(!$this->isQuoteValidForAdaptorCalculation($tokensDb, $auditLog)){
    		return false;
    	}*/
    	$totalPriceAdaptor = 0;
    	$totalNumberLed = 0;
    	$strAdaptorType = '';
    	
    	if($this->getDetail()){
    		if($this->getDetail()->getItems()){
    			$items = $this->getDetail()->getItems();
    			 
    			/* @var $item Item */
    			foreach($items as $item){
    	
    				$totalNumberLed += $item->getNumOfLedTotal();
    			}
    		}
    	}
    	
    	
    	if($totalNumberLed > 0){
    		$modByAdapter2A = $totalNumberLed % $tokensDb['%led-adaptor-2.5a-capacity%'];
    		
    		
    		
    		//find new numberOfLed by rounding up to be 75-mod-able
    		$roundedNumOfLed = ((int)($totalNumberLed / $tokensDb['%led-adaptor-2.5a-capacity%'])) * $tokensDb['%led-adaptor-2.5a-capacity%'];
    		if($modByAdapter2A != 0){
    			$roundedNumOfLed += $tokensDb['%led-adaptor-2.5a-capacity%'];
    		}
    		
    		
    		//calculate for number & price of Adaptor 5.0A (150-mod-able)
    		$numberOfAdaptor5A = (int)($roundedNumOfLed / $tokensDb['%led-adaptor-5a-capacity%']);
    		$totalPriceAdaptor = $numberOfAdaptor5A * $tokensDb['%led-adaptor-5a-rate%'];
  
    		
    		//calculate for number & price of Adaptor 2.5A (75-mod-able)
    		$numberOfAdaptor2A = 0;
    		if(($roundedNumOfLed % $tokensDb['%led-adaptor-5a-capacity%']) != 0){
    			$totalPriceAdaptor += $tokensDb['%led-adaptor-2.5a-rate%'];
    			$numberOfAdaptor2A = 1;
    		}
    		
    		$strAdaptorType = '5.0A:'.$numberOfAdaptor5A.'|'.'2.5A:'.$numberOfAdaptor2A;
    		//debug
    		//var_dump($totalNumberLed, $modByAdapter2A, $roundedNumOfLed, $totalPriceAdaptor, $strAdaptorType);
    	}
    	
    	$this->setNumOfLed($totalNumberLed);
    	$this->setPriceAdaptor($totalPriceAdaptor);
    	$this->setAdaptorType($strAdaptorType);
    	
    }
    
    private function isQuoteValidForAdaptorCalculation ($tokensDb = array(), &$auditLog = ""){
    	$result = false;
    	
    	$productId = $this->getDetail()->getProduct()->getId();
    	
    	switch($productId){
    		case 1: //acrylic
    		case 2: //inox
    		case 3: //acrynox
    			$result = true;
    			break;
    			
    		case 4: //flat-cut
    		case 5: //plaque
    		default:
    			$result = false;
    	}
    	
    	return $result;
    }
    
    public function calculatePriceTransport($tokensDb = array(), $transportRates = array(),  &$auditLog = ""){
    	
    	$multiplerRates = array();
    	$priceTransportSub = 0;
    	$priceTransportRemoteArea = 0;
    	$priceTransportFuel = 0;
    	$priceTransportSafety = 0;
    	$priceTransport = 0;

    	
    	//compare level 1 - basic mapping < 30.0
    	foreach($transportRates as $tRate){ /* @var $tRate TransportRate  */
    		
    		if($this->getWeightTransportDhl() == $tRate->getFromWeight()){
    			//var_dump($tRate);
    			$priceTransportSub = $tRate->getRate();
    		}
    		
    		if($tRate->getConditionCompare() == '<=' || $tRate->getConditionCompare() == '>'){
    			$multiplerRates[] = $tRate;
    		}
    		 
    	}
    	
    	//compare level 2 - multiplier with rates
    	foreach($multiplerRates as $tRate){
    		
    		if($this->getWeightTransportDhl() >= $tRate->getFromWeight()){ //will start if $kg >= 30
    			if($tRate->getConditionCompare() == "<=" && $this->getWeightTransportDhl()!= $tRate->getFromWeight() /*avoid duplicate adding rate from 'compare level 1', when $kg= 30*/){
    				$priceTransportSub += $tRate->getRate();
    			}else{
    				if($this->getWeightTransportDhl() <= $tRate->getToWeight()){ // case 'from < $kg < to'
    					$priceTransportSub += ($this->getWeightTransportDhl() - floor($tRate->getFromWeight())) * $tRate->getRate(); 
    				}elseif($this->getWeightTransportDhl() > $tRate->getToWeight()){ // case 'to < $kg'
    					$priceTransportSub += ($tRate->getToWeight() - floor($tRate->getFromWeight())) * $tRate->getRate(); 
    				}
    				
    				
    			}
    		}
    	}
    	
    	//calculate 3 surcharge types according to setting at company profile
    	$tmpPriceTransportSub = $priceTransportSub;
    	if($this->getCompany()){
    		
    		$c = $this->getCompany();
    		if($c->getTransportRemoteAreaSurcharge()){
    			$priceTransportRemoteArea = $tmpPriceTransportSub * $tokensDb['%transport-remote-area-surcharge%'];
    			$tmpPriceTransportSub += $priceTransportRemoteArea;
    		}
    		if($c->getTransportFuelSurcharge()){
    			$priceTransportFuel =  $tmpPriceTransportSub * $tokensDb['%transport-fuel-surcharge%'];
    			$tmpPriceTransportSub += $priceTransportFuel;
    		}
    		if($c->getTransportSafetySurcharge()){
    			$priceTransportSafety = $tmpPriceTransportSub * $tokensDb['%transport-safety-surcharge%'];
    			$tmpPriceTransportSub += $priceTransportSafety;
    		}
    	}
    	$priceTransport = $tmpPriceTransportSub;
    	
    	
    	//set properties
    	$this->setPriceTransportSub($priceTransportSub);
    	$this->setPriceTransportRemoteArea($priceTransportRemoteArea);
    	$this->setPriceTransportFuel($priceTransportFuel);
    	$this->setPriceTransportSafety($priceTransportSafety);
    	$this->setPriceTransport($priceTransport);
    	
    	//debug
    	//var_dump('WeightToCheck: '.$this->getWeightTransportDhl());
    	//var_dump($multiplerRates);
    	//var_dump('priceTransport: '.$priceTransport);
    	//exit();
    	
    	//return $priceTransport;
    }
    
    
    public function getTokens($tokensDb = array()){
    	$tokens = array(
    					
    	);
    	
    	 
    	$tokens = array_merge($tokens, $tokensDb);
    	return $tokens;
    }
    
    
    private function roundUpHalf($weight){
    	$weightTmp = (ceil($weight * 2))/2;
    
    	//var_dump('weight: '.$weight.' = '.$weightTmp);
    
    	return $weightTmp;
    }
    
    public function getWeightTransportDhl(){
    	return $this->roundUpHalf($this->weightTransport);
    }
    
    public function getVolumeTotal(){
    	return $this->getVolume() * $this->getQuantity();
    }
    
    
    
    
    
    public function getPriceSellingTotalFinalCC(){
    	return number_format($this->getPriceSellingTotalFinal() * $this->currencyRate, 2);
    }
    
    public function getPriceTransportCC(){
    	return number_format($this->getPriceTransport() * $this->currencyRate, 2);
    }
    
    public function getPriceGrandTotalCC(){
    	return number_format($this->getPriceGrandTotal() * $this->currencyRate, 2);
    	
    }
    
    public function getAdminFeeCC(){
    	return number_format($this->getAdminFee()  * $this->currencyRate,2 );
    }
    
    public function getDiscountCC(){
    	return number_format($this->getDiscount()  * $this->currencyRate,2 );
    }
    
    public function getPriceAdaptorCC(){
    	return number_format($this->getPriceAdaptor()  * $this->currencyRate,2 );
    }
    
  
    
    
    
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Quote
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Quote
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Quote
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set measureUnit
     *
     * @param string $measureUnit
     * @return Quote
     */
    public function setMeasureUnit($measureUnit)
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }

    /**
     * Get measureUnit
     *
     * @return string 
     */
    public function getMeasureUnit()
    {
        return $this->measureUnit;
    }



    /**
     * Set company
     *
     * @param \Quotemax\DashboardBundle\Entity\Company $company
     * @return Quote
     */
    public function setCompany(\Quotemax\DashboardBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Quotemax\DashboardBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set user
     *
     * @param \Quotemax\UserBundle\Entity\User $user
     * @return Quote
     */
    public function setUser(\Quotemax\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Quotemax\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set detail
     *
     * @param \Quotemax\DashboardBundle\Entity\QuoteDetail $detail
     * @return Quote
     */
    public function setDetail(\Quotemax\DashboardBundle\Entity\QuoteDetail $detail = null)
    {
        if($detail){
        	$detail->setQuote($this);
        }
    	$this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return \Quotemax\DashboardBundle\Entity\QuoteDetail 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Quote
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Quote
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Quote
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    


    /**
     * Set currencyRate
     *
     * @param string $currencyRate
     * @return Quote
     */
    public function setCurrencyRate($currencyRate)
    {
        $this->currencyRate = $currencyRate;

        return $this;
    }

    /**
     * Get currencyRate
     *
     * @return string 
     */
    public function getCurrencyRate()
    {
        return $this->currencyRate;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Quote
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceTransport
     *
     * @param string $priceTransport
     * @return Quote
     */
    public function setPriceTransport($priceTransport)
    {
        $this->priceTransport = $priceTransport;

        return $this;
    }

    /**
     * Get priceTransport
     *
     * @return string 
     */
    public function getPriceTransport()
    {
        return $this->priceTransport;
    }



 


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Quote
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Quote
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
    
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 * 
	 */
	public function updateLifecycle() {
		if(!$this->createdAt) $this->createdAt = new \DateTime();
		$this->modifiedAt = new \DateTime();
	}	

    /**
     * Set margin
     *
     * @param string $margin
     * @return Quote
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return string 
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * Set additionalCost
     *
     * @param string $additionalCost
     * @return Quote
     */
    public function setAdditionalCost($additionalCost)
    {
        $this->additionalCost = $additionalCost;

        return $this;
    }

    /**
     * Get additionalCost
     *
     * @return string 
     */
    public function getAdditionalCost()
    {
        return $this->additionalCost;
    }

    /**
     * Set priceSelling
     *
     * @param string $priceSelling
     * @return Quote
     */
    public function setPriceSelling($priceSelling)
    {
        $this->priceSelling = $priceSelling;

        return $this;
    }

    /**
     * Get priceSelling
     *
     * @return string 
     */
    public function getPriceSelling()
    {
        return $this->priceSelling;
    }

    /**
     * Set priceSellingTotalAll
     *
     * @param string $priceSellingTotalAll
     * @return Quote
     */
    public function setPriceSellingTotalAll($priceSellingTotalAll)
    {
        $this->priceSellingTotalAll = $priceSellingTotalAll;

        return $this;
    }

    /**
     * Get priceSellingTotalAll
     *
     * @return string 
     */
    public function getPriceSellingTotalAll()
    {
        return $this->priceSellingTotalAll;
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return Quote
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set adminFee
     *
     * @param string $adminFee
     * @return Quote
     */
    public function setAdminFee($adminFee)
    {
        $this->adminFee = $adminFee;

        return $this;
    }

    /**
     * Get adminFee
     *
     * @return string 
     */
    public function getAdminFee()
    {
        return $this->adminFee;
    }

    /**
     * Set priceSellingTotalFinal
     *
     * @param string $priceSellingTotalFinal
     * @return Quote
     */
    public function setPriceSellingTotalFinal($priceSellingTotalFinal)
    {
        $this->priceSellingTotalFinal = $priceSellingTotalFinal;

        return $this;
    }

    /**
     * Get priceSellingTotalFinal
     *
     * @return string 
     */
    public function getPriceSellingTotalFinal()
    {
        return $this->priceSellingTotalFinal;
    }

    /**
     * Set volume
     *
     * @param string $volume
     * @return Quote
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return string 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set weightTransport
     *
     * @param string $weightTransport
     * @return Quote
     */
    public function setWeightTransport($weightTransport)
    {
        $this->weightTransport = $weightTransport;

        return $this;
    }

    /**
     * Get weightTransport
     *
     * @return string 
     */
    public function getWeightTransport()
    {
        return $this->weightTransport;
    }

    /**
     * Set priceGrandTotal
     *
     * @param string $priceGrandTotal
     * @return Quote
     */
    public function setPriceGrandTotal($priceGrandTotal)
    {
        $this->priceGrandTotal = $priceGrandTotal;

        return $this;
    }

    /**
     * Get priceGrandTotal
     *
     * @return string 
     */
    public function getPriceGrandTotal()
    {
        return $this->priceGrandTotal;
    }

    /**
     * Set priceTransportSub
     *
     * @param string $priceTransportSub
     * @return Quote
     */
    public function setPriceTransportSub($priceTransportSub)
    {
        $this->priceTransportSub = $priceTransportSub;

        return $this;
    }

    /**
     * Get priceTransportSub
     *
     * @return string 
     */
    public function getPriceTransportSub()
    {
        return $this->priceTransportSub;
    }

    /**
     * Set priceTransportRemoteArea
     *
     * @param string $priceTransportRemoteArea
     * @return Quote
     */
    public function setPriceTransportRemoteArea($priceTransportRemoteArea)
    {
        $this->priceTransportRemoteArea = $priceTransportRemoteArea;

        return $this;
    }

    /**
     * Get priceTransportRemoteArea
     *
     * @return string 
     */
    public function getPriceTransportRemoteArea()
    {
        return $this->priceTransportRemoteArea;
    }

    /**
     * Set priceTransportFuel
     *
     * @param string $priceTransportFuel
     * @return Quote
     */
    public function setPriceTransportFuel($priceTransportFuel)
    {
        $this->priceTransportFuel = $priceTransportFuel;

        return $this;
    }

    /**
     * Get priceTransportFuel
     *
     * @return string 
     */
    public function getPriceTransportFuel()
    {
        return $this->priceTransportFuel;
    }

    /**
     * Set priceTransportSafety
     *
     * @param string $priceTransportSafety
     * @return Quote
     */
    public function setPriceTransportSafety($priceTransportSafety)
    {
        $this->priceTransportSafety = $priceTransportSafety;

        return $this;
    }

    /**
     * Get priceTransportSafety
     *
     * @return string 
     */
    public function getPriceTransportSafety()
    {
        return $this->priceTransportSafety;
    }


    /**
     * Set statusPayment
     *
     * @param boolean $statusPayment
     * @return Quote
     */
    public function setStatusPayment($statusPayment)
    {
        $this->statusPayment = $statusPayment;

        return $this;
    }

    /**
     * Get statusPayment
     *
     * @return boolean 
     */
    public function getStatusPayment()
    {
        return $this->statusPayment;
    }

    /**
     * Set numOfLed
     *
     * @param integer $numOfLed
     * @return Quote
     */
    public function setNumOfLed($numOfLed)
    {
        $this->numOfLed = $numOfLed;

        return $this;
    }

    /**
     * Get numOfLed
     *
     * @return integer 
     */
    public function getNumOfLed()
    {
        return $this->numOfLed;
    }

    /**
     * Set priceAdaptor
     *
     * @param string $priceAdaptor
     * @return Quote
     */
    public function setPriceAdaptor($priceAdaptor)
    {
        $this->priceAdaptor = $priceAdaptor;

        return $this;
    }

    /**
     * Get priceAdaptor
     *
     * @return string 
     */
    public function getPriceAdaptor()
    {
        return $this->priceAdaptor;
    }

    /**
     * Set adaptorType
     *
     * @param string $adaptorType
     * @return Quote
     */
    public function setAdaptorType($adaptorType)
    {
        $this->adaptorType = $adaptorType;

        return $this;
    }

    /**
     * Get adaptorType
     *
     * @return string 
     */
    public function getAdaptorType()
    {
        return $this->adaptorType;
    }

    /**
     * Set confirmedAt
     *
     * @param \DateTime $confirmedAt
     * @return Quote
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    /**
     * Get confirmedAt
     *
     * @return \DateTime 
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * Set paidAt
     *
     * @param \DateTime $paidAt
     * @return Quote
     */
    public function setPaidAt($paidAt)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * Get paidAt
     *
     * @return \DateTime 
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * Set trackingNo
     *
     * @param string $trackingNo
     * @return Quote
     */
    public function setTrackingNo($trackingNo)
    {
        $this->trackingNo = $trackingNo;

        return $this;
    }

    /**
     * Get trackingNo
     *
     * @return string 
     */
    public function getTrackingNo()
    {
        return $this->trackingNo;
    }

    /**
     * Set deliveredAt
     *
     * @param \DateTime $deliveredAt
     * @return Quote
     */
    public function setDeliveredAt($deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    /**
     * Get deliveredAt
     *
     * @return \DateTime 
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }
}
