<?php


namespace Quotemax\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Quotemax\DashboardBundle\Enum\DesignTypeEnum;

/**
 * @ORM\Entity(repositoryClass="Quotemax\DashboardBundle\Repository\QuoteDetailRepository")
 * @ORM\Table(name="quote_detail")
 * 
 */
class QuoteDetail
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Quotemax\DashboardBundle\Entity\Quote", inversedBy="detail")
     * @ORM\JoinColumn(name="quote_id", referencedColumnName="id")
     * 
     */
    protected $quote;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Product")
     *
     */
    protected $product;
    


    
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * //options ['letter', 'logo', 'both']
     * //probably change to patternType
     */
    protected $designType;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $letter;
    
    
    /**
     * @ORM\Column(type="integer",nullable=true)
     *
     */
    protected $numOfLetterItem;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    protected $numOfGraphicItem;
    
    

    
    /**
     * @ORM\OneToMany(targetEntity="ItemOption", mappedBy="quoteDetail", cascade={"persist", "remove"})
     * 
     * //each will render differnt product option categories with ites optionValues
     * //will inherite into their child elements
     *
     */
    protected $itemOptions;
   

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="quoteDetail", cascade={"persist", "remove"})
     * 
     *
     */
    protected $items;
    
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Document", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="quote_document",
 	 *      joinColumns={@ORM\JoinColumn(name="quote_id", referencedColumnName="quote_id")},
 	 *      inverseJoinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")})
     *
     */
    protected $documents;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Document", cascade={"all"})
     * @ORM\JoinTable(name="quote_otherdocument",
     *      joinColumns={@ORM\JoinColumn(name="quote_id", referencedColumnName="quote_id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")})
     *
     */
    protected $otherDocuments;
	
	

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $remark;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    protected $scaleOption;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $otherScale;
	
	
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itemOptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setNumOfGraphicItem(0);
        $this->designType = DesignTypeEnum::LETTER;
        
        $this->scaleOption = 1;
    }


    /**
     * Set designType
     *
     * @param string $designType
     * @return QuoteDetail
     */
    public function setDesignType($designType)
    {
        $this->designType = $designType;

        return $this;
    }

    /**
     * Get designType
     *
     * @return string 
     */
    public function getDesignType()
    {
        return $this->designType;
    }

    /**
     * Set letter
     *
     * @param string $letter
     * @return QuoteDetail
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter
     *
     * @return string 
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set numOfLetterItem
     *
     * @param integer $numOfLetterItem
     * @return QuoteDetail
     */
    public function setNumOfLetterItem($numOfLetterItem)
    {
        $this->numOfLetterItem = $numOfLetterItem;

        return $this;
    }

    /**
     * Get numOfLetterItem
     *
     * @return integer 
     */
    public function getNumOfLetterItem()
    {
        return $this->numOfLetterItem;
    }

    /**
     * Set numOfGraphicItem
     *
     * @param integer $numOfGraphicItem
     * @return QuoteDetail
     */
    public function setNumOfGraphicItem($numOfGraphicItem)
    {
        $this->numOfGraphicItem = $numOfGraphicItem;

        return $this;
    }

    /**
     * Get numOfGraphicItem
     *
     * @return integer 
     */
    public function getNumOfGraphicItem()
    {
        return $this->numOfGraphicItem;
    }

 

    /**
     * Set quote
     *
     * @param \Quotemax\DashboardBundle\Entity\Quote $quote
     * @return QuoteDetail
     */
    public function setQuote(\Quotemax\DashboardBundle\Entity\Quote $quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Get quote
     *
     * @return \Quotemax\DashboardBundle\Entity\Quote 
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * Add itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     * @return QuoteDetail
     */
    public function addItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
    	if($itemOptions){
    		$itemOptions->setQuoteDetail($this);
    	}
        $this->itemOptions[] = $itemOptions;

        return $this;
    }

    /**
     * Remove itemOptions
     *
     * @param \Quotemax\DashboardBundle\Entity\ItemOption $itemOptions
     */
    public function removeItemOption(\Quotemax\DashboardBundle\Entity\ItemOption $itemOptions)
    {
        $this->itemOptions->removeElement($itemOptions);
    }

    /**
     * Get itemOptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemOptions()
    {
        return $this->itemOptions;
    }

    /**
     * Add items
     *
     * @param \Quotemax\DashboardBundle\Entity\Item $items
     * @return QuoteDetail
     */
    public function addItem(\Quotemax\DashboardBundle\Entity\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Quotemax\DashboardBundle\Entity\Item $items
     */
    public function removeItem(\Quotemax\DashboardBundle\Entity\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set product
     *
     * @param \Quotemax\DashboardBundle\Entity\Product $product
     * @return QuoteDetail
     */
    public function setProduct(\Quotemax\DashboardBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Quotemax\DashboardBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add documents
     *
     * @param \Quotemax\DashboardBundle\Entity\Document $documents
     * @return QuoteDetail
     */
    public function addDocument(\Quotemax\DashboardBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \Quotemax\DashboardBundle\Entity\Document $documents
     */
    public function removeDocument(\Quotemax\DashboardBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return QuoteDetail
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string 
     */
    public function getRemark()
    {
        return $this->remark;
    }
    
    
    
    
    
    /**
     * Set scaleOption
     *
     * @param integer scaleOption
     * @return Quote
     */
    public function setScaleOption($scaleOption)
    {
    	$this->scaleOption = $scaleOption;
    
    	return $this;
    }
    
    /**
     * Get scaleOption
     *
     * @return integer
     */
    public function getScaleOption()
    {
    	return $this->scaleOption;
    }
    
    
    /**
     * Set otherScale
     *
     * @param string $otherScale
     * @return QuoteDetail
     */
    public function setOtherScale($otherScale)
    {
    	$this->otherScale = $otherScale;
    
    	return $this;
    }
    
    /**
     * Get otherScale
     *
     * @return string
     */
    public function getOtherScale()
    {
    	return $this->otherScale;
    }
    
    
    

    /**
     * Add otherDocuments
     *
     * @param \Quotemax\DashboardBundle\Entity\Document $otherDocuments
     * @return QuoteDetail
     */
    public function addOtherDocument(\Quotemax\DashboardBundle\Entity\Document $otherDocuments)
    {
        $this->otherDocuments[] = $otherDocuments;

        return $this;
    }

    /**
     * Remove otherDocuments
     *
     * @param \Quotemax\DashboardBundle\Entity\Document $otherDocuments
     */
    public function removeOtherDocument(\Quotemax\DashboardBundle\Entity\Document $otherDocuments)
    {
        $this->otherDocuments->removeElement($otherDocuments);
    }

    /**
     * Get otherDocuments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOtherDocuments()
    {
        return $this->otherDocuments;
    }
}
