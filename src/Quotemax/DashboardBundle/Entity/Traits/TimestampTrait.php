<?php

namespace Quotemax\DashboardBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;


trait TimestampTrait
{
	/**
	 * 
	 * @ORM\Column(type="datetime")
	 * @var \DateTime 
	 */
	private $createdAt;
	
	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $modifiedAt;
	
	
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updateLifecycle() {
		if(!$this->createdAt) $this->createdAt = new \DateTime();
		$this->modifiedAt = new \DateTime();
	}	
	
	
   /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Quote
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Quote
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
	
	
}
