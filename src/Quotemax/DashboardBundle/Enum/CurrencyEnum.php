<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class CurrencyEnum extends Enum
{
  	const EUR = 'eur';
  	const USD = 'usd';

  	
  	public static function getPossibleValues()
  	{
  		return array(static::EUR, 
  					static::USD,
  					);
  	}
  	
  	public static function getReadables()
  	{
  		return array(static::EUR => 'EUR', 
  					static::USD => 'USD',
  					);
  	}
}
