<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class DesignTypeEnum extends Enum
{
  	const LETTER = 'lt';
  	const LOGO = 'lg';
  	const BOTH = 'bh';
  	
  	public static function getPossibleValues()
  	{
  		return array(static::LETTER, 
  					static::LOGO, 
  					static::BOTH);
  	}
  	
  	public static function getReadables()
  	{
  		return array(static::LETTER => 'Letter', 
  					static::LOGO => 'Logo',
  					static::BOTH => 'Both (Letter and Logo)');
  	}
}
