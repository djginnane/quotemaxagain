<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class QuoteStatusEnum extends Enum
{
  	const PENDING = 'PEN'; //new pending
  	const CONFIRM = 'CON'; //confirm order by client (for confirming to issue purchasing order)
  	const EXPIRE = 'EXP'; //expired / cancelled quote
  	const PAID = 'PAD'; //paid invoice
  	const DELIVER = 'DLV'; //delivered products to client, tagging with DSL ref no.
  	
  	public static function getPossibleValues()
  	{
  		return array(static::PENDING, 
  					static::CONFIRM, 
  					static::EXPIRE,
  					static::PAID,
  					static::DELIVER
  					);
  	}
  	
  	public static function getReadables()
  	{
  		return array(
  					static::PENDING => 'New Pending', 
  					static::CONFIRM => 'Confirmed', 
  					static::EXPIRE => 'Expired / Cancelled',
  					static::PAID => 'Paid',
  					static::DELIVER => 'Shipped'
  					);
  	}
}
