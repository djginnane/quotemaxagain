<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class UnitTypeEnum extends Enum
{
  	const METRIC = 'metric';
  	const IMPERIAL = 'imperial';
  	
  	public static function getPossibleValues()
  	{
  		return array(static::METRIC, 
  					static::IMPERIAL, 
  					
  					);
  	}
  	
  	public static function getReadables()
  	{
  		return array(static::METRIC => 'cm.', 
  					static::IMPERIAL => 'inches',
  					);
  	}
}
