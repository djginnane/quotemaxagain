<?php

namespace Quotemax\DashboardBundle\Enum;

use Biplane\EnumBundle\Enumeration\Enum;
use Symfony\Component\Security\Core\User\UserInterface;



class UserRolesEnum extends Enum
{
  	const ROLE_USER_DEFAULT = 'ROLE_USER';
  	const ROLE_ADMIN = 'ROLE_ADMIN';
  	const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
  	
  	public static function getPossibleValues()
  	{
  		return array(
  					
  					static::ROLE_ADMIN, 
  					static::ROLE_SUPER_ADMIN,
  					static::ROLE_USER_DEFAULT
  			);
  	}
  	
  	public static function getReadables()
  	{
  		return array(
  					
  					static::ROLE_ADMIN => 'Power User',
  					static::ROLE_SUPER_ADMIN => 'Super Admin',
  					static::ROLE_USER_DEFAULT => 'Company User'
  		);
  	}
}
