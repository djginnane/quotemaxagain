<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Entity\Variable;
use Quotemax\DashboardBundle\Form\Type\CategoryValueType;

class CategoryValuesType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('valueFieldType' => 'text'))
    {
        $this->options = $options;
    }
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('categoryValues', 'bootstrap_collection', array('label' => false, 
											'required' => false,
											'type' => new CategoryValueType($this->options),
											'allow_add'          => false,
											'allow_delete'       => false,
											'add_button_text'    => 'Add Element',
											'delete_button_text' => 'Delete Element',
											'sub_widget_col'     => 12,
											'button_col'         => 0,
											'options'            => array(
													'attr' => array('style' => 'inline')
											)
											))
		//Button
		->add('saveChanges', 'submit', array('label' => 'Save Changes'))
		->add('cancel', 'button', array('label' => 'Cancel'))
		;
	}

	public function getName()
	{
		return 'variables';
	}
	
	
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Models\CategoryValuesModel',
				'valueFieldType' => 'text',
		));
	}
	
}