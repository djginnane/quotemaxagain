<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;

class DocumentType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		//->add('name', null, array('label' => 'File Name', 'required' => false))
		->add('file', null, array('label' => 'File', 'required' => true))

		
		;
	}

	public function getName()
	{
		return 'document';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Document',
		));
	}
}