<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;

class ItemOptionType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('remark', 'hidden', array('label' => 'Remark', 'required' => false))
		;
		
		
		$builder
		->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
			$itemOption = $event->getData();
			$form = $event->getForm();
		
			if($itemOption){
			if($itemOption->getCategory()){
				$category = $itemOption->getCategory();
				
				//ToDo if possible configure at DB
				$option = array('multiple' => false, 'expanded' => false);
				switch($category->getSlug()){
					case 'options-front':
					case 'options-return':
					case 'special-design': //id: 11
					case 'mounting': //id: 13
					case 'mounting-1': //id: 23
					case 'mounting-2': //id: 27
					case 'option': //id: 14
					case 'options': //id: 18
						$option['multiple'] = false;
						$option['expanded'] = true;
						$option['empty_value'] = 'none';
						break;
					case 'illuminated-area':
					case 'raw-material': //id: 12
					case 'raw-material-1': //id: 21
					case 'raw-material-2': //id: 25
					case 'front-cover-material': //id: 16
						$option['multiple'] = false;
						$option['expanded'] = false;
						$option['empty_value'] = false;
						break;
					default:
						$option['multiple'] = false;
						$option['expanded'] = true;
						$option['empty_value'] = false;
				}
				
				$class = '';
				switch ($category->getMethodOfChoice()){
					case 'M': //'Mandatory' category have only one value & must be calculated un-optionally
						$class = 'hide';
						$option['multiple'] = false;
						$option['expanded'] = false; //must be select drop-down only to make sure select one item as value
						break;
					case 'O':
						$class = '';
						break;
					default:
						$class = '';
				}
				
				$form->add('categoryValue', 'entity', array('label' => $category->getName(),
						'required' => false,
						'class' => 'QuotemaxDashboardBundle:CategoryValue',
						'property' => 'name',
						'query_builder' => function (EntityRepository $er) use ($category){
							return $er->createQueryBuilder('cv')
							->where ('cv.category = :id')->setParameter('id', $category)
							->OrderBy('cv.id', 'ASC');
						},
						'multiple' => $option['multiple'],
						'expanded' => $option['expanded'],
						'empty_value' => false,
						'attr' => array(
							'rel' => $category->getName(),
							'class' => 'col-lg-9 '.$class //'hide'
							//'class' => 'col-lg-9 '.$class //'hide'
						),
						'label_attr' => array(
							'class' => 'col-lg-2 '.$class
							//'class' => 'col-lg-2 '.$class		
						),
						'empty_value' => $option['empty_value'],
						//'empty_data' => null,
						//'data' => null
				));
				
			}
			}
		});
	}

	public function getName()
	{
		return 'itemOption';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\ItemOption',
		));
	}
}