<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;

class ItemType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', null, array('label' => 'Name', 'required' => true, 'attr' => array( 'title' => 'Element Name')))
		
		->add('height', null, array('label' => 'Height', 'required' => true, 'attr' => array( 'title' => '# Height')))
		->add('width', null, array('label' => 'Width', 'required' => true, 'attr' => array( 'title' => '# Width')))
		->add('depth', null, array('label' => 'Depth', 'required' => true, 'attr' => array( 'title' => '# Depth')))
		->add('perimeter', null, array('label' => 'Perimeter', 'required' => true, 'attr' => array( 'title' => '# Perimeter')))
		->add('surface', null, array('label' => 'Surface', 'required' => false, 'attr' => array( 'title' => '# Surface')))
		->add('type', 'hidden', array('label' => 'Type', 'required' => false, 'attr' => array( 'title' => '# type')))
		

		
		;
	}

	public function getName()
	{
		return 'item';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Item',
		));
	}
}