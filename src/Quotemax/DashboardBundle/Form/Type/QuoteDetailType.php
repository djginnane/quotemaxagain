<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;
use Quotemax\DashboardBundle\Entity\ItemOption;
use Quotemax\DashboardBundle\Form\Type\ItemOptionType;
use Quotemax\DashboardBundle\Enum\DesignTypeEnum;
class QuoteDetailType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('product', 'entity', array('label' => 'Product Type', 'required' => false,
											'class' => 'Quotemax\DashboardBundle\Entity\Product',
											'property' => 'name',
											'multiple' => false,
											'expanded' => true,
											'empty_value' => false,
											'attr' => array('class' => 'form-inline'),
											'label_attr' => array('class' => 'required')
											))
		->add('designType', 'choice', array('label' => 'Sign Type', 'required' => false, 
											'choices' => DesignTypeEnum::getReadables(),
											//'data' => 'lt',
											'multiple' => false,
											'expanded' => true,
											'empty_value' => false,
											'attr' => array('class' => 'form-inline'),
											'label_attr' => array('class' => 'required')
											))
		->add('letter', null, array('label' => 'Letters', 'required' => false))
		->add('letterRepeat', null, array('label' => 'Letters (re-type)', 'required' => false, 'mapped'=>false))
		->add('numOfGraphicItem', 'integer', array('label' => 'Logo Elements', 'required' => false, 'attr' => array('min'=>0, 'step'=>1)))

		->add('documents', 'bootstrap_collection', array('label' => 'Document',//'Document(s)', 
											'required' => true,
											'type' => new DocumentType(),
											'allow_add'          => true,
											'allow_delete'       => true,
											'add_button_text'    => 'Add Document',
											'delete_button_text' => 'Delete Document',
											'sub_widget_col'     => 9,
											'button_col'         => 3,
											'options'            => array(
													'attr' => array('style' => 'inline')
											)
											))
		->add('otherDocuments', 'bootstrap_collection', array('label' => 'Other File(s)',
				'required' => false,
				'type' => new DocumentType(),
				'allow_add'          => true,
				'allow_delete'       => true,
				'add_button_text'    => 'Add Document',
				'delete_button_text' => 'Delete Document',
				'sub_widget_col'     => 9,
				'button_col'         => 3,
				'options'            => array(
						'attr' => array('style' => 'inline')
				)
		))
		
		
		->add('itemOptions', 'bootstrap_collection', array('label' => false, 
											'required' => false,
											'type' => new ItemOptionType(),
											'allow_add'          => true,
											'allow_delete'       => true,
											'add_button_text'    => 'Add Element',
											'delete_button_text' => 'Delete Element',
											'sub_widget_col'     => 12,
											'button_col'         => 0,
											'options'            => array(
													'attr' => array('style' => 'inline')
											)
											))
		
		
		->add('items', 'bootstrap_collection', array('label' => 'Element (for letters or graphic logos)', 
											'required' => true,
											'type' => new ItemType(),
											'allow_add'          => true,
								            'allow_delete'       => true,
								            'add_button_text'    => 'Add Element',
								            'delete_button_text' => 'Delete Element',
								            'sub_widget_col'     => 12,
								            'button_col'         => 0,
											'options'            => array(
													'attr' => array('style' => 'inline')
											)
											))
		
		->add('remark', 'textarea', array('label' => 'Requirement', 'required' => false))
		
		->add('scaleOption', 'choice', array(	'label' => 'Scale', 'required' => false,
												'choices' => [1 => 'At scale 1/1', 0 => 'Other scale'],
												//'data' => 'lt',
												'multiple' => false,
												'expanded' => true,
												'empty_value' => false,
												'attr' => array('class' => 'form-inline'),
												'label_attr' => array('class' => '')
				
										))
		->add('otherScale', 'text', array('label' => 'Other Scale', 'required' => false))
		
		;
	}

	public function getName()
	{
		return 'quoteDetail';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\QuoteDetail',
		));
	}
}