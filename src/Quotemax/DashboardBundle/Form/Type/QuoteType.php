<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\Locale\Locale;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;
use Doctrine\ORM\EntityRepository;

class QuoteType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('companyId' => ''))
	{
	
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$optionsCustom = $this->options;
		$builder
		->add('company', 'entity', array('label' => 'Company name <br>(for admin only)', 'required' => true, 
													'class' => 'Quotemax\DashboardBundle\Entity\Company',
													'empty_value' => 'select company',
													'query_builder' => function (EntityRepository $er) use ($optionsCustom){
														$qb = $er->createQueryBuilder('i')->OrderBy('i.id', 'ASC');
														$qb->leftJoin('i.users', 'u'); //'UserDetail' model
														$qb->leftJoin('u.user', 'um'); // 'User' model
														$qb->where('1=1');
														
														//make sure company w/o user will not be include into dropdown
														$qb->andWhere('u IS NOT NULL');
														
														//not include internal companies
														$qb->andWhere('i.internal != 1');
														
														//not include companies under power user
														$qb->andWhere('um.roles NOT LIKE :role')
															->setParameter('role', '%ADMIN%')
														;
														
														if(!empty($optionsCustom['companyId'])){
															$qb->andWhere('i.id = :companyId')
																->setParameter('companyId', $optionsCustom['companyId']);
														}
														
														return $qb;
													},
										)) //ToDO: if admin then display company list
		//->add('code', null, array('label' => 'Quotation #', 'required' => false))
		->add('name', null, array('label' => 'Project', 'required' => true))
		//->add('description', 'textarea', array('label' => 'Information', 'required' => false)) //REMOVED BY CLIENT REQUEST
		->add('quantity', 'integer', array('label' => 'Quantity', 'required' => false, 'attr' => array('min'=>1, 'step'=>1)))
		->add('currency', 'choice', array('label' => 'Currency', 
											'required' => false, 
											'choices' => CurrencyEnum::getReadables(),
											'empty_value' => false,
											))
		->add('measureUnit', 'choice', array('label' => 'Unit', 
												'required' => false, 
												'choices' => UnitTypeEnum::getReadables(),
												'empty_value' => false
												))
		
		->add('detail', new QuoteDetailType(), array('label' => 'Quote Detail'))
		
		//Button
		->add('saveChanges', 'submit', array('label' => 'Validate'))
		->add('cancel', 'button', array('label' => 'Cancel'))
		->add('nextStep', 'button', array('label' => 'Next Step', 'attr' => array('class' => 'btn-primary quote_nextStep')))
		->add('backEdit', 'button', array('label' => 'Back / Edit', 'attr' => array('class' => 'quote_backEdit')))
		
		;
	}

	public function getName()
	{
		return 'quote';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Quote',
		));
	}
}