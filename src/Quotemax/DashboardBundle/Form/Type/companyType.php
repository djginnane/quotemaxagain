<?php
namespace Quotemax\DashboardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Quotemax\DashboardBundle\Entity\UserDetail;
use Quotemax\DashboardBundle\Enum\ActivityTypeEnum;
use Quotemax\DashboardBundle\Enum\UnitTypeEnum;
use Quotemax\DashboardBundle\Enum\CurrencyEnum;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;

class companyType extends AbstractType
{
	private $options = array();
	
	public function __construct(array $options = array('locale' => 'en'))
	{
	
		$this->options = $options;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$builder
		//->add('clientCode', null, array('label' => 'Client Code','attr' => array('maxlength' => 4)))
		->add('name', null, array('label' => 'Company Name'))
		->add('type', 'choice', array('label' => 'Activity', 'required' => false,'choices' => ActivityTypeEnum::getReadables(), 'empty_value' => false))
		->add('billingAddress', new LocationType($this->options), array('label' => 'Billing Address', 'required' => true))
		->add('isSameAsBilling', null, array('label' => 'Use same address as billing'))
		->add('shippingAddress', new LocationType($this->options), array('label' => 'Shipping Address', 'required' => true))
		->add('contactName', null, array('label' => 'Contact Name', 'required' => true))
		->add('phoneNumber', null, array('label' => 'Phone Number', 'required' => true))
		->add('faxNumber', null, array('label' => 'Fax Number', 'required' => false))
		->add('mobileNumber', null, array('label' => 'Mobile Number', 'required' => false))
		->add('email', 'email', array('label' => 'E-mail', 'required' => true))
		->add('website', null, array('label' => 'Website', 'required' => false))
		->add('instantMessaging', null, array('label' => 'Instant Messaging', 'required' => false))
		->add('unit', 'choice', array('label' => 'Units', 'required' => false, 'choices' => UnitTypeEnum::getReadables(), 'empty_value' => false))
		->add('currency', 'choice', array('label' => 'Currency', 'required' => false, 'choices' => CurrencyEnum::getReadables(), 'empty_value' => false))
		->add('users', 'collection', array('label' => 'Login / Password', 'type' => new UserDetailType($this->options)))
		->add('discount', null, array('label' => 'Discount', 'required' => false, 'attr' => array(
										        'input_group' => array('append' => '%')
										    )))
		->add('adminFeeEur', null, array('label' => false, 'required' => false, 'attr' => array(
								        'input_group' => array('append' => 'EUR &euro;')
								    )))
		->add('adminFeeUsd', null, array('label' => false, 'required' => false, 'attr' => array(
								        'input_group' => array('append' => 'USD $')
								    )))
		->add('homeMessageEn', 'textarea', array('label' => 'Attention Required (English)', 'required' => false))
		->add('homeMessageFr', 'textarea', array('label' => 'Attention Required (French)', 'required' => false))
		
		//transport fee
		->add('transportRemoteAreaSurcharge', 'checkbox', array('label' => 'Remote Area Surcharge', 'required' => false))
		//->add('transportFuelSurcharge', 'checkbox', array('label' => 'Fuel Surcharge', 'required' => false))
		->add('transportSafetySurcharge', 'checkbox', array('label' => 'Safety Surcharge', 'required' => false))
		//Button
		->add('saveChanges', 'submit', array('label' => 'Save Changes'))
		->add('cancel', 'button', array('label' => 'Cancel'))
		;
		
		
		$builder
		->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
			$company = $event->getData();
			$form = $event->getForm();
		
			if($company){
				if($company->getId()){ // 'edit/update' case
					//REMOVED
					/*$form
					->add('clientCode', null, array('label' => 'Client Code', 'disabled' => true ,'attr' => array('maxlength' => 4)));
					*/
					if(array_key_exists('isCompanyUser', $this->options)){
						if($this->options['isCompanyUser']){
							$form->remove('discount');
							$form->remove('adminFeeEur');
							$form->remove('adminFeeUsd');
							$form->remove('homeMessageEn');
							$form->remove('homeMessageFr');
							$form->remove('transportRemoteAreaSurcharge');
							$form->remove('transportSafetySurcharge');
						}
					}
				}
			}
		});
		
		
		
		
		
	}

	public function getName()
	{
		return 'company';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Quotemax\DashboardBundle\Entity\Company',
		));
	}
}