<?php

namespace Quotemax\DashboardBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
	/* //customize tempalte for knpMenuBundle
	 *  - http://bootstrap.braincrafted.com/getting-started.html#template (customize bootstrap template for form widget, knp_menu, knp_paginator)
	 * 	- http://nielsmouthaan.nl/symfony2-knpmenubundle-font-awesome-twitter-bootstrap-integration-navigation-bar-including-icons-and-dropdown-menus/
	 *  - http://stackoverflow.com/questions/11226963/customize-the-knpmenubundle
	 * 	- https://github.com/KnpLabs/KnpMenuBundle/blob/master/Resources/doc/custom_renderer.md
	 *  - http://nielsmouthaan.nl/symfony2-knpmenubundle-font-awesome-twitter-bootstrap-integration-navigation-bar-including-icons-and-dropdown-menus/
	 *  - http://thewebmason.com/tag/knpmenubundle/
	 *  - >> add icon into menu, http://nielsmouthaan.nl/symfony2-knpmenubundle-font-awesome-twitter-bootstrap-integration-navigation-bar-including-icons-and-dropdown-menus/
	 *  
	 *  
	 *  **check if user roles is granted / authenticatecd yet or not
	 *  - http://stackoverflow.com/questions/10271570/how-to-check-if-an-user-is-logged-in-symfony2-inside-a-controller
	 */
    public function mainMenu(FactoryInterface $factory, array $options)
    {
    	//get required services
    	//$session = $this->container->get('session');
    	$security = $this->container->get('security.context');
    	$currentUser = $security->getToken()->getUser();
    	$request = $this->container->get('request');
    	$translator =  $this->container->get('translator');
    	
    	
    	//set menu
        $menu = $factory->createItem('root');
        //$menu->setChildrenAttribute('class', 'navbar navbar-inverse  navbar-default');

        $menu->addChild('Home', array('route' => 'qmxDashboard_default_index' /*'_root'*/, 'label'=> $translator->trans('Home')))
        	->setAttribute('icon', 'fa fa-home fa-fw');
        $menu->addChild('Quote', array('route' => 'qmxDashboard_quote_index', 'label'=> $translator->trans('New Quotation')))
        	->setAttribute('icon', 'fa fa-copy fa-fw');
        $menu->addChild('Order Confirmation', array('route' => 'qmxDashboard_order_index', 'label'=> $translator->trans('Order')))
        	->setAttribute('icon', 'fa fa-shopping-cart fa-fw'); //fa-thumb-tack 
        $menu->addChild('Orders', array('route' => 'qmxDashboard_history_index', 'label'=> $translator->trans('Follow-Up')))
        	->setAttribute('icon', 'fa fa-thumbs-o-up  fa-fw'); //
        
        if ($security->isGranted('ROLE_SUPER_ADMIN')) {
        	$menu->addChild('Reports', array('route' => 'qmxDashboard_report_index', 'label'=> $translator->trans('Reports')))
        		->setAttribute('icon', 'fa fa-area-chart fa-fw');
        }
        
        if ($security->isGranted('ROLE_ADMIN')) {
        	
        	$menu->addChild('Admin', array('route' => 'qmxDashboard_admin_index', 'label'=> $translator->trans('Admin')))
        		->setAttribute('icon', 'fa fa-cogs fa-fw');
        }
        
        $userIcon = 'fa-users';
        if ($security->isGranted('ROLE_ADMIN')) {
        	$userIcon = 'fa-user-plus';
        }
        $menu->addChild('CompanyName', array(
        						'label' => $currentUser->getUsername(),
        						//'icon' => 'fa fa-user fa-fw'
        						))
        						->setAttribute('icon', 'fa '.$userIcon.' fa-fw');
        
       
       
        /*
        $menu->addChild('cart', array(
        		'route' => 'cultural_store_web_homepage',
        		'linkAttributes' => array('title' => 'Voir Panier'),
        		'labelAttributes' => array('icon' => 'icon-user')))->setLabel('Voir Panier');
        */
        //working with menu tree
        if (!$security->isGranted('ROLE_ADMIN')) {
        	$menu['CompanyName']->addChild('Profile', array('route' => 'qmxDashboard_company_update', 'label'=> $translator->trans('Profile')))->setAttribute('divider_append', true)
        		->setAttribute('icon', 'fa fa-users fa-lg');
        }
        $menu['CompanyName']->addChild('localeEn', array('route' => 'qmxDashboard_default_locale', 'routeParameters' => array('locale' => 'en'), 
        													'label'=> $translator->trans('en-locale')))
        					->setAttribute('icon', 'fa fa-language fa-1');
    	$menu['CompanyName']->addChild('LocaleFr', array('route' => 'qmxDashboard_default_locale', 'routeParameters' => array('locale' => 'fr'), 
    														'label'=> $translator->trans('fr-locale')))
    						->setAttribute('icon', 'fa fa-language fa-1');
        
        $menu['CompanyName']->addChild('Logout', array('route' => 'fos_user_security_logout', 'label'=> $translator->trans('Logout')))
        					->setAttribute('icon', 'fa fa-sign-out fa-1');
        
      
        //set active menu
        //$menu['Quote']->setCurrent(true);
        //$menu->setCurrentUri($this->container->get('request')->getRequestUri());
        //$menu['Home']->setCurrent(true);

        switch($request->get('_route')) {
        	case "qmxDashboard_default_index":
        		$menu['Home']->setCurrent(true);
        		break;
        	case "qmxDashboard_admin_company_list":
        	case "qmxDashboard_admin_company_create":
        	case "qmxDashboard_admin_company_update":
        	case "qmxDashboard_admin_transport_import":
        	case "qmxDashboard_admin_currency_update":
        	case "qmxDashboard_admin_message_update":	
        		$menu['Admin']->setCurrent(true);
        		break;
        	case "qmxDashboard_quote_index":
        	case "qmxDashboard_quote_list":
        	case "qmxDashboard_quote_create":
        	case "qmxDashboard_quote_update":
        	case "qmxDashboard_quote_confirm":
        		$menu['Quote']->setCurrent(true);
        		break;
        	case "qmxDashboard_history_index":
        	case "qmxDashboard_history_list":
        	case "qmxDashboard_history_create":
        	case "qmxDashboard_history_update":
        	case "qmxDashboard_quote_view_invoice":
        		$menu['Orders']->setCurrent(true);
        		break;
        	case "qmxDashboard_order_index":
        	case "qmxDashboard_order_list":
        	case "qmxDashboard_order_create":
        	case "qmxDashboard_order_update":
        		$menu['Order Confirmation']->setCurrent(true);
        		break;
        	case "qmxDashboard_report_index":
        	case "qmxDashboard_report_view":
        		$menu['Reports']->setCurrent(true);
        		break;
        	default:
        }
        

        return $menu;
    }
    
    public function localeMenu(FactoryInterface $factory, array $options)
    {
    	$menu = $factory->createItem('Locale Switch');
    	$menu->addChild('en', array('route' => 'qmxUser_locale_switch', 'routeParameters' => array('locale' => 'en')));
    	$menu->addChild('fr', array('route' => 'qmxUser_locale_switch', 'routeParameters' => array('locale' => 'fr')));
    	//$menu->setChildrenAttributes(array('class' => 'btn btn-default'));
    	
    	return $menu;
    	
    }
    
    public function adminMenu(FactoryInterface $factory, array $options)
    {
    	//set service
    	$request = $this->container->get('request');
    	$translator =  $this->container->get('translator');
    	$security = $this->container->get('security.context');
    	
    	//set menu
    	$menu = $factory->createItem('QuoteMax Admin');

    	//$menu->addChild('QuoteMax Admin');
    	
    	
    	if (!$security->isGranted('ROLE_SUPER_ADMIN')) {
    		$menu->addChild('Companies Management', array('route' => 'qmxDashboard_admin_company_list', 'label'=> $translator->trans('Profile(s) Management')));
    	}
    	
    	if ($security->isGranted('ROLE_SUPER_ADMIN')) {
    		$menu->addChild('User Management', array('route' => 'qmxDashboard_admin_user_list', 'label'=> $translator->trans('User(s) Management')));
    		$menu->addChild('Product Rate', array('route' => 'qmxDashboard_admin_product_rate_update', 'label'=> $translator->trans('Material Rate')));
    		$menu->addChild('Product Cofficient', array('route' => 'qmxDashboard_admin_product_coefficient_update', 'label'=> $translator->trans('Product Coefficient (Led)')));
    		$menu->addChild('Transport Rates', array('route' => 'qmxDashboard_admin_transport_import', 'label'=> $translator->trans('Transportation Rate')));
    		$menu->addChild('Transport Surcharges', array('route' => 'qmxDashboard_admin_transport_surcharge_update', 'label'=> $translator->trans('Transportation Charge')));
    		$menu->addChild('Currency Rates', array('route' => 'qmxDashboard_admin_currency_update', 'label'=> $translator->trans('Currency Rate')));
    		$menu->addChild('Margin Rates', array('route' => 'qmxDashboard_admin_margin_rate_update', 'label'=> $translator->trans('Profit Margin Rate')));
    		$menu->addChild('Labour Rates', array('route' => 'qmxDashboard_admin_labour_rate_update', 'label'=> $translator->trans('Labour Rate')));
    	}
    	
    	$menu->addChild('Homepage Messages', array('route' => 'qmxDashboard_admin_message_update', 'label'=> $translator->trans('Homepage Message')));
    	$menu->addChild('General', array('route' => 'qmxDashboard_admin_general_update', 'label'=> $translator->trans('General Setting')));
    	 
    	
    	
    	//add sub-menu
    	
    	
    	
    	//set active menu
    	//$menu->setCurrentUri($this->container->get('request')->getRequestUri());
    	switch($request->get('_route')) {
        	case "qmxDashboard_admin_company_create":
        	case "qmxDashboard_admin_company_update":
        		if(array_key_exists('Companies Management', $menu)){
    				$menu['Companies Management']->setCurrent(true);
        		}
    			break;
    		case "qmxDashboard_admin_user_create":
    		case "qmxDashboard_admin_user_update":
    				$menu['User Management']->setCurrent(true);
    				break;
    		case "qmxDashboard_admin_company_update":
    			$menu['Transport Rates']->setCurrent(true);
    			break;
    		case "qmxDashboard_admin_currency_update":
    			$menu['Currency Rates']->setCurrent(true);
    			break;
    		case "qmxDashboard_admin_margin_rate_update":
    			$menu['Margin Rates']->setCurrent(true);
    			break;
    		case "qmxDashboard_admin_labour_rate_update":
    			$menu['Labour Rates']->setCurrent(true);
    			break;
    		case "qmxDashboard_admin_message_update":
    			$menu['Homepage Messages']->setCurrent(true);
    			break;
    		
    		default:
    			
    	}
    	 
    	return $menu;
    	 
    }
    
    
    
}