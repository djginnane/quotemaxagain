<?php

namespace Quotemax\DashboardBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
	//Fixtures are used to load a controlled set of data into a database. 
	//This data can be used for testing or could be the initial data required for the application to run smoothly
	//  - http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Fabien');

        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }
}
