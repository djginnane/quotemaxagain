<?php

namespace Quotemax\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('QuotemaxUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
