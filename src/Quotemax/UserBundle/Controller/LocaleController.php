<?php

namespace Quotemax\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("/locale", name="qmxDashboard_locale")
 */
class LocaleController extends Controller
{
	/*
    public function indexAction($name)
    {
        return $this->render('QuotemaxUserBundle:Default:index.html.twig', array('name' => $name));
    }
    */
    
    /**
     * @Route("/{locale}", name="qmxUser_locale_switch", requirements={"locale" = "en|fr"})
     *
     */
    public function localeAction(Request $request, $locale)
    {
    	
    
    	$localeCurrent = $request->getLocale();
    	if($locale != $localeCurrent){
    		$request->setLocale($locale);
    		$request->getSession()->set('_locale', $locale);
    		
    	}
    	$referer = $this->getRequest()->headers->get('referer');
    	return new RedirectResponse($referer);
    }
}
