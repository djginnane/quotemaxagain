<?php


namespace Quotemax\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Quotemax\DashboardBundle\Entity\UserDetail;
use Quotemax\DashboardBundle\Enum\UserRolesEnum;

/**
 * @ORM\Entity(repositoryClass="Quotemax\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="user_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    
    /**
     * @ORM\OneToOne(targetEntity="Quotemax\DashboardBundle\Entity\UserDetail", 
     * 						mappedBy="user", cascade={"persist", "remove", "merge"})
     * 
     */
    protected $detail;
    
   
    

    public function __construct()
    {
        parent::__construct();
        $this->setRoles(array(UserRolesEnum::ROLE_USER_DEFAULT));
        // your own logic
        //$detail = new UserDetail();
        //$this->setDetail($detail);
        
    }
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

 

    /**
     * Set detail
     *
     * @param \Quotemax\DashboardBundle\Entity\UserDetail $detail
     * @return User
     */
    public function setDetail(\Quotemax\DashboardBundle\Entity\UserDetail $detail = null)
    {
        $this->detail = $detail;
		if($detail){
       	 $this->detail->setUser($this);
		}
        
        return $this;
    }

    /**
     * Get detail
     *
     * @return \Quotemax\DashboardBundle\Entity\UserDetail 
     */
    public function getDetail()
    {
        return $this->detail;
    }
}
