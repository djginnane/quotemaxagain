<?php

namespace Quotemax\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class QuotemaxUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
